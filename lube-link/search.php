<?php include ('header.php'); ?>
<div id="facetsContainer">
    <div class="facetSearchTop">
    <?php $results = getSearchResuts();
    //echo '<pre>';print_r($results); echo '</pre>'; 
    $equipment_list = $results['equipment_list'];
    $total_matches = $equipment_list['@attributes']['total_matches'];
    $page_size = $equipment_list['@attributes']['page_size'];
    $page = $equipment_list['@attributes']['page'];
    $searchString = getCurrentQueryUrl();
    parse_str($searchString, $searchParams);
    //echo '<pre>';print_r($searchParams);echo '</pre>';
    ?>
        <script type="text/javascript">
        $(document).ready(function(){
            // $("#modelSearchButton input").prop('disabled', true);

            // $("#modelSearchInput input").keyup(function(){
            //     var length = $(this).val().length;
            //     if ( length > 0) {
            //     $("#modelSearchButton input").prop('disabled', false);
            //     } else {
            //     $("#modelSearchButton input").prop('disabled', true);
            //     }
            // });
        });
        </script>

        <form name="keyword_search" method="get" action="<?php echo os_base_url(); ?><?php echo isset($_GET['lang']) ? $_GET['lang'] . '/search/' : 'search/'; ?>">
            <div class="modelSearchContainer facetDropdown">
                <!--div id="modelSearchText" class=" modelSearchBlock"><p>Find the right motor oil for your vehicle</p></div-->
                <?php $browse = getBrowseList(); //echo '<pre>';print_r(getCurrentQueryUrl());echo '</pre>'; ?>
                <div id="modelSearchInput" class="modelSearchBlock">
                    <input class="text" name="q" type="text" size="30" value="<?php echo (isset($searchParams['q']) && !empty($searchParams['q'])) ? $searchParams['q'] : ''; ?>" />
                </div>
                <div id="modelSearchSelect" class="modelSearchBlock os-select">
                    <select id="familygroup" name="familygroup">
                        <option <?php echo (isset($searchParams['familygroup']) && $searchParams['familygroup'] == '') ? 'selected' : ''; ?> value=""><?php esc_html_e( $LANGUAGE['OS_INPUT_BROWSE_OPTION_ALL'], 'lube-link' ); ?></option>
                        <?php if(!empty($browse) && isset($browse['response']['index']['item'])) {
                            foreach($browse['response']['index']['item'] as $item) { ?>
                            <option value="<?php echo $item; ?>" <?php echo (isset($searchParams['familygroup']) && $searchParams['familygroup'] == $item) ? 'selected' : ''; ?>><?php echo $item; ?></option>
                        <?php }
                        } ?>
                    </select>
                </div>
                <div id="modelSearchButton" class="modelSearchBlock os-home-search-btn">
                    <input class="fa button" type="submit" onmouseover="this.className='fa button buttonhover'" onmouseout="this.className='fa button'" value="">
                </div>
                <br>
            </div>
        </form>


        <script type="text/javascript">
        $(document).ready(function(){
            $(".searchFacets select").change(function(){
            window.location.href=$(this).val();
            });

            $(".prevButton").click(function(){
            prevPage = $("input[name=page]").val();
            $("input[name=page]").val(prevPage - 2);
            });
        });
        </script>
        
        <?php if($total_matches > 0): ?>
        <?php $facets = $results['facets']['facet']; 
        $facetsTitle = [
            'fueltype' => $LANGUAGE['OS_FILTER_FACET_TYPE_FUELTYPE'],
            'year'     => $LANGUAGE['OS_FILTER_FACET_TYPE_YEAR'],
            'series'   => $LANGUAGE['OS_FILTER_FACET_TYPE_SERIES'],
            'displacement' => $LANGUAGE['OS_FILTER_FACET_TYPE_DISPLACEMENT'],
            'manufacturer' => $LANGUAGE['OS_FILTER_FACET_TYPE_MANUFACTURER'],
            'family' => $LANGUAGE['OS_FILTER_FACET_TYPE_FAMILY'] ,
            'familygroup' => $LANGUAGE['OS_FILTER_FACET_TYPE_FAMILYGROUP'] 
        ];
        ?>
        <h2 class="clear filter-title os-title"><?php echo $LANGUAGE['OS_SEARCH_PAGE_TITLE']; ?></h2>
        <div class="searchFacets">
        <?php if(!empty($facets)): ?>
            <?php if(!isset($facets['@attributes']['name'])): ?>
            <?php foreach($facets as $facet): ?>
            <?php $options = $facet['bucket']; ?>
                <div class="facet <?php echo $facet['@attributes']['name']; ?> os-select">
                    <select class="filter-facet" id="<?php echo $facet['@attributes']['name']; ?>" name="<?php echo $facet['@attributes']['name']; ?>">
                        <option value="<?php echo os_base_url(); ?><?php echo isset($_GET['lang']) ? $_GET['lang'] . '/' : ''; ?>search/?q=<?php echo urlencode($searchParams['q']); ?>&amp;familygroup=<?php echo urlencode($searchParams['familygroup']) ?>"><?php echo $facetsTitle[$facet['@attributes']['name']]; ?></option>
                        <?php foreach($options as $option): ?>
                            <?php $optionName = trim($facet['@attributes']['name']);
                                $optionValue = $option['@attributes']['value'];
                                $filterSelected = (isset($searchParams[$optionName]) && $optionValue == $searchParams[$optionName]) ? 'selected' : ''; 
                                $facetSearchParams = $searchParams;
                                $facetSearchParams[$optionName] = $optionValue;
                                $query = http_build_query($facetSearchParams, '', '&amp;');
                            ?>
                            <option value="<?php echo os_base_url(); ?><?php echo isset($_GET['lang']) ? $_GET['lang'] . '/' : ''; ?>search/?<?php echo $query; ?>" <?php echo $filterSelected; ?>><?php echo $optionValue ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            <?php endforeach; ?>
            <?php else: ?>
                <?php $options = $facets['bucket'];//echo '<pre>';print_r($options);echo '</pre>'; ?>
            <div class="facet <?php echo $facets['@attributes']['name']; ?> os-select">
                <select class="filter-facet" id="<?php echo $facets['@attributes']['name']; ?>" name="<?php echo $facets['@attributes']['name']; ?>">
                    <option value="<?php echo os_base_url(); ?><?php echo isset($_GET['lang']) ? $_GET['lang'] . '/' : ''; ?>search/?q=<?php echo urlencode($searchParams['q']); ?>&amp;familygroup=<?php echo urlencode($searchParams['familygroup']) ?>"><?php echo $facetsTitle[$facets['@attributes']['name']]; ?></option>
                    <?php foreach($options as $option): ?>
                        <?php $optionName = trim($facets['@attributes']['name']);
                            $optionValue = $option['@attributes']['value'];
                            $filterSelected = (isset($searchParams[$optionName]) && $optionValue == $searchParams[$optionName]) ? 'selected' : ''; 
                            $facetSearchParams = $searchParams;
                            $facetSearchParams[$optionName] = $optionValue;
                            $query = http_build_query($facetSearchParams, '', '&amp;');
                        ?>
                        <option value="<?php echo os_base_url(); ?><?php echo isset($_GET['lang']) ? $_GET['lang'] . '/' : ''; ?>search/?<?php echo $query; ?>" <?php echo $filterSelected; ?>><?php echo $optionValue ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <?php endif; ?>
        <?php endif; ?>
        </div>
        <?php endif; ?>
    </div>
    <div class="facetSearchBottom">
        <?php if($total_matches > 0 && (isset($searchParams['fueltype']) || isset($searchParams['year']) || isset($searchParams['series']) || isset($searchParams['displacement']) || isset($searchParams['manufacturer']) || isset($searchParams['family']))): ?>
        <div class="clearFacetContainer">
        <?php //$facets = $results['facets']['facet']; 
            $facetsTitle = [
                'fueltype' => $LANGUAGE['OS_FILTER_FACET_TYPE_FUELTYPE'],
                'year'     => $LANGUAGE['OS_FILTER_FACET_TYPE_YEAR_CLEAR'],
                'series'   => $LANGUAGE['OS_FILTER_FACET_TYPE_SERIES'],
                'displacement' => $LANGUAGE['OS_FILTER_FACET_TYPE_DISPLACEMENT'],
                'manufacturer' => $LANGUAGE['OS_FILTER_FACET_TYPE_MANUFACTURER'],
                'family' => $LANGUAGE['OS_FILTER_FACET_TYPE_FAMILY']
            ];
            
        ?>
        <h3><?php echo $LANGUAGE['OS_FILTERED_BY_TXT']; ?></h3>
        <?php 
        foreach($searchParams as $key => $value): 
            $facetSearchParams = $searchParams;
            if($key != 'q' && $key != 'familygroup') {
                $facetName = $key;
            
                if(isset($facetSearchParams[$facetName])) {
                    unset($facetSearchParams[$facetName]);
                }

                $query = http_build_query($facetSearchParams, '', '&amp;');
        ?>
            <a class="clearFacet" href="<?php echo os_base_url(); ?><?php echo isset($_GET['lang']) ? $_GET['lang'] . '/' : ''; ?>search/?<?php echo $query; ?>">
                <span><?php echo $facetsTitle[$facetName] ?>: <?php echo $value; ?></span>
                <span class="clearSymbol"><i class="fa fa-times-circle" aria-hidden="true"></i></span>
            </a>
            <?php } ?>
        <?php endforeach; ?>
            <a class="clearFacetAll" href="<?php echo os_base_url(); ?><?php echo isset($_GET['lang']) ? $_GET['lang'] . '/' : ''; ?>search/?q=<?php echo urlencode($searchParams['q']); ?>&amp;familygroup=<?php echo urlencode($searchParams['familygroup']) ?>"><?php echo $LANGUAGE['OS_BTN_CLEAR_FILTER_TXT']; ?></a>
        </div>
        <?php endif; ?>
        <div class="searchMessage <?php echo ($total_matches == 0) ? 'no-results' : ''; ?>">
            <!-- Prev page -->
            <?php $next_page = $page + 1; ?>
            <?php if($total_matches > 0 && $page <= ceil($total_matches/$page_size)): ?>
                
                <form name="prev_page" class="prev_page" method="get" action="<?php echo os_base_url(); ?><?php echo isset($_GET['lang']) ? $_GET['lang'] . '/search/' : 'search/'; ?>">
                    <input type="hidden" name="q" value="<?php echo (isset($searchParams['q']) && !empty($searchParams['q'])) ? $searchParams['q'] : ''; ?>" />
                    <input type="hidden" name="familygroup" value="<?php echo (isset($searchParams['familygroup'])) ? $searchParams['familygroup'] : ''; ?>" />
                    <input type="hidden" name="page" value="<?php echo $page > 2 ? $page - 1 : 1; ?>">
                    <div class="searchPageControls">
                        <button class="button btn-prev" type="submit" value="<?php echo $LANGUAGE['OS_RESULT_BTN_PREV_TXT']; ?>" onmouseover="javascript:this.className='button buttonhover'" onmouseout="javascript:this.className='button'" <?php if(!isset($searchParams['page']) || (isset($searchParams['page']) && $searchParams['page'] == 1)): ?>disabled<?php endif; ?>>
                            <i class="fa fa-arrow-left" aria-hidden="true"></i>
                        </button>
                    </div>
                </form>
                
                <!-- Next page -->
                
                <?php if($page <= ceil($total_matches/$page_size)): ?>
                <form name="next_page" class="next_page" method="get" action="<?php echo os_base_url(); ?><?php echo isset($_GET['lang']) ? $_GET['lang'] . '/search/' : 'search/'; ?>">
                    
                    <input type="hidden" name="q" value="<?php echo (isset($searchParams['q']) && !empty($searchParams['q'])) ? $searchParams['q'] : ''; ?>" />
                    <input type="hidden" name="familygroup" value="<?php echo (isset($searchParams['familygroup'])) ? $searchParams['familygroup'] : ''; ?>" />
                    <input type="hidden" name="page" value="<?php echo $next_page; ?>"/>
                    <div class="searchPageControls">
                        <p><?php echo $LANGUAGE['OS_RESULT_SHOWING_TXT']; ?> 
                        <?php echo ($total_matches > 0 && $page == 1) ? 1 : ($page - 1)*$page_size + 1; ?>
                        - 
                        <?php echo $page*$page_size > $total_matches ? $total_matches : $page*$page_size; ?> <?php echo $LANGUAGE['OS_RESULT_OF_TXT']; ?> 
                        <?php echo $total_matches; ?> 
                        <?php echo $LANGUAGE['OS_RESULT_RESULTS_TXT']; ?>.</p>
                        <?php if($next_page <= ceil($total_matches/$page_size)): ?>
                        <button class="button btn-next" type="submit" value="<?php echo $LANGUAGE['OS_RESULT_BTN_NEXT_TXT']; ?>" onmouseover="javascript:this.className='button buttonhover'" onmouseout="javascript:this.className='button'">
                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                        </button>
                        <?php endif; ?>
                    </div>
                </form>
                <?php endif; ?>
            <?php elseif($total_matches == 0): ?>
                <div class="noResults">
                    <p><?php echo $LANGUAGE['OS_RESULT_EMPTY_TXT']; ?></p>
                    <ul class="searchControls">
                        <li>
                            <a href="<?php echo os_base_url(); ?>"><?php echo $LANGUAGE['OS_BTN_NEW_SEARCH_TXT']; ?></a>
                        </li>
                        <li>
                            <a href="https://klondikelubricants.com/contact-klondike/" target="_BLANK"><?php echo $LANGUAGE['OS_RESULT_NOT_VEHICLE_TXT']; ?></a>
                        </li>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<div id="facetResultsTable">
<?php if($total_matches > 0): 
//echo '<pre>';print_r($equipment_list);echo '</pre>';
$urlLang = isset($_GET['lang']) ? '/' . $_GET['lang'] : '';
$equipments = $equipment_list['equipment']; ?>
    <table class="vehicle" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th class="category"><?php echo $LANGUAGE['OS_FILTER_RESULTS_TABLE_HEAD_CATEGORY'] ?></th>
                <th class="manufacturer"><?php echo $LANGUAGE['OS_FILTER_RESULTS_TABLE_HEAD_MANUFACTURER'] ?></th>
                <th class="model"><?php echo $LANGUAGE['OS_FILTER_RESULTS_TABLE_HEAD_MODEL'] ?></th>
                <th class="year"><?php echo $LANGUAGE['OS_FILTER_RESULTS_TABLE_HEAD_YEAR'] ?></th>
                <th class="fuel"><?php echo $LANGUAGE['OS_FILTER_RESULTS_TABLE_HEAD_FUEL'] ?></th>
            </tr>
        </thead>
        <tbody>
        <?php if(!isset($equipments['@attributes']) && $page <= ceil($total_matches/$page_size)):
            $i = 0; foreach($equipments as $equipment) { ?>
            <tr class="row<?php echo ($i%2) ? 2 : 1 ?>">
                <td class="category" data-th="<?php echo $LANGUAGE['OS_FILTER_RESULTS_TABLE_HEAD_CATEGORY'] ?>"><?php echo $equipment['family']; ?></td>
                <td class="manufacturer" data-th="<?php echo $LANGUAGE['OS_FILTER_RESULTS_TABLE_HEAD_MANUFACTURER'] ?>"><?php echo $equipment['manufacturer']; ?></td>
                <td class="model" data-th="<?php echo $LANGUAGE['OS_FILTER_RESULTS_TABLE_HEAD_MODEL'] ?>"><a href="<?php echo os_base_url(false) . $urlLang . $equipment['@attributes']['href']; ?>"><?php echo $equipment['display_name_short']; ?></a></td>
                <td class="year" data-th="<?php echo $LANGUAGE['OS_FILTER_RESULTS_TABLE_HEAD_YEAR'] ?>"><?php echo $equipment['display_year']; ?></td>
                <td class="fuel" data-th="<?php echo $LANGUAGE['OS_FILTER_RESULTS_TABLE_HEAD_FUEL'] ?>"><?php echo $equipment['fueltype']; ?></td>
            </tr>
        <?php $i++; } ?>
        <?php elseif($page <= ceil($total_matches/$page_size)): ?>
            <tr class="row1">
                <td class="category" data-th="<?php echo $LANGUAGE['OS_FILTER_RESULTS_TABLE_HEAD_CATEGORY'] ?>"><?php echo $equipments['family']; ?></td>
                <td class="manufacturer" data-th="<?php echo $LANGUAGE['OS_FILTER_RESULTS_TABLE_HEAD_MANUFACTURER'] ?>"><?php echo $equipments['manufacturer']; ?></td>
                <td class="model" data-th="<?php echo $LANGUAGE['OS_FILTER_RESULTS_TABLE_HEAD_MODEL'] ?>"><a href="<?php echo os_base_url(false) . $urlLang . $equipments['@attributes']['href']; ?>"><?php echo $equipments['display_name_short']; ?></a></td>
                <td class="year" data-th="<?php echo $LANGUAGE['OS_FILTER_RESULTS_TABLE_HEAD_YEAR'] ?>"><?php echo $equipments['display_year']; ?></td>
                <td class="fuel" data-th="<?php echo $LANGUAGE['OS_FILTER_RESULTS_TABLE_HEAD_FUEL'] ?>"><?php echo $equipments['fueltype']; ?></td>
            </tr>
        <?php else: ?>
            <tr class="row1">
                <td colspan="5"><?php echo $LANGUAGE['OS_RESULT_EMPTY_TXT'] ?></td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
<?php endif; ?>
</div>
<?php include ('footer.php'); ?>   