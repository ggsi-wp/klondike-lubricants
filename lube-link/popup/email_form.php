<?php 
$equipment = getEquipmentDetail();
$lang = !empty($_GET['lang']) ? $_GET['lang'] . '/' : '';
$equipmentDetail = $equipment['equipment'];
//echo '<pre>';print_r($equipment);echo '</pre>';
?>
<div id="content">
    <table style="width:100%">
        <tbody>
            <tr>
                <td><input class="button" onmouseover="this.className = 'button buttonhover'" onmouseout="this.className = 'button'" type="button" onclick="window.close()" value="Close Window"></td>
            </tr>
        </tbody>
    </table>
    <?php if(isset($_POST['g-recaptcha-response']) && !empty(($_POST['g-recaptcha-response'])) && !empty($_POST['email']) && !empty($_POST['title'])) {
        //echo '<pre>';print_r($_POST);echo '</pre>';
        //if(wp_verify_nonce($_POST['oil_search_form_nonce'],'oil_search_sendemail')) {
            $email = $_POST['email'];
            $title = $_POST['title'];
            $bcc = $_POST['bcc']; 
            $email_body = '';
            $boundary=md5(uniqid(rand()));
            ob_start();    
        ?>
        <div id="printLogo"><a href="<?php echo os_base_url(); ?>" title="<?php echo $LANGUAGE['OS_PAGE_TITLE']; ?>"><img src="cid:klondike-logo-uid" alt="<?php echo $LANGUAGE['OS_PAGE_TITLE']; ?>"/></a></div>
        <table style="width:100%;color:#000;border-spacing: 0;">
            <tbody>
                <tr style="color:#000">
                    <td style="color:#000;padding: 0;">
                        <table style="border-spacing: 0;float:left;color:#000;padding: 0;" align="left">
                            <tbody>
                                <tr style="color:#000">
                                    <td style="color:#000;padding: 0;">
                                        <table style="border-spacing: 0;float:left;color:#000;padding: 0;" align="left">
                                            <tbody>
                                                <tr style="color:#000">
                                                    <td style="color:#000;padding: 0;">
                                                        <div style="color:#000;float:left">
                                                        <h3 style="color:#000;float:left;font-size:30px;line-height:35px;margin:0 20px 0 0;"><?php echo $equipmentDetail['display_name_short']; ?></h3>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table style="border-spacing: 0;float:left;clear:left;color:#000" align="left">
                                            <tbody>
                                                    <tr style="color:#000">
                                                    <td style="color:#000;padding: 0;">
                                                        <span style="color:#000;font-weight:700;float:left;clear:left"><?php echo $equipmentDetail['manufacturer']; ?></span>
                                                    </td>
                                                </tr>
                                                <tr style="color:#000">
                                                    <td style="color:#000;padding: 0;">
                                                        <span style="color:#000;font-weight:700;float:left;clear:left"><?php echo $equipmentDetail['model']; ?></span>
                                                    </td>
                                                </tr>
                                                <tr style="color:#000">
                                                    <td style="color:#000;padding: 0;">
                                                        <span style="color:#000;font-weight:700;float:left;clear:left"><?php echo $equipmentDetail['alt_fueltype']; ?></span>
                                                    </td>
                                                </tr>
                                                <tr style="color:#000">
                                                    <td style="color:#000;padding: 0;">
                                                        <span style="color:#000;font-weight:700;float:left;clear:left"><?php echo $equipmentDetail['display_year']; ?></span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td style="color:#000;padding: 0;">
                        <table style="float:right;color:#000" align="right">
                            <tbody>
                                <tr style="color:#000">
                                    <td style="color:#000;padding: 0;">
                                        <div style="color:#000;float:right;width:200px">
                                        <?php $resources = $equipment['equipment']['application'][0]['product'][0]; 
                                        if(!empty($resources['resource'])):
                                            foreach($resources['resource'] as $resource) {
                                                $attribute = isset($resource['@attributes']) ? $resource['@attributes'] : '';
                                                if(!empty($attribute) && $attribute['type'] == 'packshot') { ?>
                                                <img src="<?php echo $attribute['href'] ?>" alt="<?php echo $resources['name'] ?>" />
                                            <?php }
                                            }
                                        endif; 
                                        ?>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <table style="color:#000;width:100%;margin-bottom:20px;clear:both;border-collapse:collapse">
            <thead>
                <tr>
                    <th class="application" style="color:#000;font-weight:700;width:10%;padding:10px 20px;border:1px solid #000"><div style="color:#000"><?php echo $LANGUAGE['OS_POPUP_PRINT_APPLICATION_TXT']; ?></div></th>
                    <th colspan="2" class="recommendation" style="color:#000;font-weight:700;width:40%;padding:10px 20px;border:1px solid #000"><div style="color:#000"><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TXT']; ?></div></th>
                    <th class="capacity" style="color:#000;font-weight:700;padding:10px 20px;border:1px solid #000;width:5%;"><div style="color:#000"><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_CAPACITY_TXT']; ?> (gal)</div></th>
                    <th class="datasheet" style="color:#000;font-weight:700;padding:10px 20px;border:1px solid #000;width:10%;"><div style="color:#000"><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_DATASHEET_TXT']; ?></div></th>
                </tr>
            </thead>
            <tbody>
            <?php $applications = $equipment['equipment']['application']; //echo '<pre>';print_r($applications);echo '</pre>';
            if(!empty($applications)){
                if(!isset($applications['display_name'])):
                $a = 0; foreach($applications as $application) { 
                    // if($application['display_name'] != 'Brake & Clutch Fluid' && $application['display_name'] != 'Power Steering' && $application['display_name'] != 'Brake Fluid' && strpos($application['display_name'], 'Coolant') === FALSE): //KL-72
                    if(strpos($application['display_name'], 'Coolant') === FALSE):    
                ?>
                <?php if(!empty($application['product'])): ?>
                    <?php if(isset($application['product'][0])) {
                    $i = 0; foreach($application['product'] as $product) { ?>
                    <tr class="row<?php echo ($a%2) ? 2 : 1 ?> prodrow<?php echo ($i%2) ? 2 : 1 ?>">
                        <?php if($i == 0){ ?>
                            <td class="application" rowspan="<?php echo count($application['product']) ?>" style="color:#000;width:10%;padding:10px 20px;border:1px solid #000">
                                <div><?php echo $application['display_name']; ?></div>
                            </td>
                            <td class="tiername tiernamet1" style="color:#000;white-space:nowrap;padding:10px 20px;border:1px solid #000;width: 10%;">
                                <div><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_BEST_TXT']; ?></div>
                            </td>
                            <td class="recommendation" style="color:#000;width:40%;padding:10px 20px;border:1px solid #000"><?php echo isset($product['name']) ? $product['name'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_NO_PRODUCT_TXT']; ?></td>
                            <td class="capacity" rowspan="<?php echo count($application['product']) ?>" style="color:#000;padding:10px 20px;border:1px solid #000">
                                <div><?php echo isset($application['display_capacity']) ? $application['display_capacity'] : ''; ?></div>
                            </td>
                            <td class="datasheet" style="color:#000;padding:10px 20px;border:1px solid #000">
                                <?php if(isset($product['resource']) && !empty(($product['resource']))): ?>
                                    <div class="links">
                                        <?php foreach($product['resource'] as $resource): 
                                                if(isset($resource['@attributes']) && $resource['@attributes']['type'] != 'packshot'){
                                                ?>
                                                <div class="datasheet datasheet-<?php echo $resource['@attributes']['type']; ?>">
                                                    <a href="<?php echo $resource['@attributes']['href']; ?>" title="<?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?>" target="_blank"><?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?></a>
                                                </div>
                                            <?php } 
                                        endforeach; ?>
                                    </div>
                                <?php endif; ?>
                            </td>
                        <?php } else { ?>
                            <td class="tiername tiernamet2" style="color:#000;white-space:nowrap;padding:10px 20px;border:1px solid #000;width: 10%;">
                                <div>
                                    <?php 
                                        if(count($application['product']) > 2) {
                                            if($i == 1) {
                                                echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_BETTER_TXT'];
                                            } else {
                                                echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_GOOD_TXT'];
                                            }
                                        } else {
                                            echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_GOOD_TXT'];
                                        } 
                                    ?>
                                </div>
                            </td>
                            <td class="recommendation" style="color:#000;width:40%;padding:10px 20px;border:1px solid #000"><?php echo isset($product['name']) ? $product['name'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_NO_PRODUCT_TXT']; ?></td>
                            <td class="datasheet" style="color:#000;padding:10px 20px;border:1px solid #000">
                                <?php if(isset($product['resource']) && !empty(($product['resource']))): ?>
                                    <div class="links">
                                        <?php foreach($product['resource'] as $resource): 
                                                if(isset($resource['@attributes']) && $resource['@attributes']['type'] != 'packshot'){
                                                ?>
                                                <div class="datasheet datasheet-<?php echo $resource['@attributes']['type']; ?>">
                                                    <a href="<?php echo $resource['@attributes']['href']; ?>" title="<?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?>" target="_blank"><?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?></a>
                                                </div>
                                            <?php } 
                                        endforeach; ?>
                                    </div>
                                <?php endif; ?>
                            </td>
                        <?php } ?>
                    </tr>
                    <?php $i++; } ?>
                    <?php } elseif(isset($application['product']['name'])) { 
                        $i = 0; $product = $application['product'];
                        //echo '<pre>';print_r($application['product']);echo '</pre>';
                    ?>
                    <tr class="row<?php echo ($a%2) ? 2 : 1 ?> prodrow<?php echo ($i%2) ? 2 : 1 ?>">
                        <td class="application" rowspan="<?php //echo count($product) ?>" style="color:#000;width:10%;padding:10px 20px;border:1px solid #000">
                            <div><?php echo $application['display_name']; ?></div>
                        </td>
                        <td class="tiername tiernamet1" style="color:#000;white-space:nowrap;padding:10px 20px;border:1px solid #000;width: 10%;">
                            <div><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_BEST_TXT']; ?></div>
                        </td>
                        <td class="recommendation" style="color:#000;width:40%;padding:10px 20px;border:1px solid #000"><?php echo isset($product['name']) ? $product['name'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_NO_PRODUCT_TXT']; ?></td>
                        <td class="capacity" style="color:#000;padding:10px 20px;border:1px solid #000">
                            <div><?php echo isset($application['display_capacity']) ? $application['display_capacity'] : ''; ?></div>
                        </td>
                        <td class="datasheet" style="color:#000;padding:10px 20px;border:1px solid #000">
                            <?php if(isset($product['resource']) && !empty(($product['resource']))): ?>
                                <div class="links">
                                    <?php foreach($product['resource'] as $resource): 
                                            if(isset($resource['@attributes']) && $resource['@attributes']['type'] != 'packshot'){
                                            ?>
                                            <div class="datasheet datasheet-<?php echo $resource['@attributes']['type']; ?>">
                                                <a href="<?php echo $resource['@attributes']['href']; ?>" title="<?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?>" target="_blank"><?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?></a>
                                            </div>
                                        <?php } 
                                    endforeach; ?>
                                </div>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <?php } else { ?>
                    <tr class="row<?php echo ($a%2) ? 2 : 1 ?> prodrow1">
                        <td class="application" rowspan="1">
                            <div><?php echo $application['display_name']; ?></div>
                        </td>
                        <td class="recommendation" colspan="2"><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_NO_PRODUCT_TXT']; ?></td>
                        <td class="capacity" rowspan="1">
                            <div><?php echo isset($application['display_capacity']) ? $application['display_capacity'] : ''; ?></div>
                        </td>
                        <td class="datasheet" style="color:#000;padding:10px 20px;border:1px solid #000"></td>
                    </tr>
                    <?php } ?>
                <?php else: ?>
                    <tr class="row<?php echo ($a%2) ? 2 : 1 ?> prodrow1">
                        <td class="application" rowspan="1" style="color:#000;width:10%;padding:10px 20px;border:1px solid #000">
                            <div><?php echo $application['display_name']; ?></div>
                        </td>
                        <td class="recommendation" colspan="2" style="color:#000;padding:10px 20px;border:1px solid #000"><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_NO_PRODUCT_TXT']; ?></td>
                        <td class="capacity" rowspan="1" style="color:#000;padding:10px 20px;border:1px solid #000">
                            <div><?php echo isset($application['display_capacity']) ? $application['display_capacity'] : ''; ?></div>
                        </td>
                        <td class="datasheet" style="color:#000;padding:10px 20px;border:1px solid #000">
                            <?php if(isset($product['resource']) && !empty(($product['resource']))): ?>
                                <div class="links">
                                    <?php foreach($product['resource'] as $resource): 
                                            if(isset($resource['@attributes']) && $resource['@attributes']['type'] != 'packshot'){
                                            ?>
                                            <div class="datasheet datasheet-<?php echo $resource['@attributes']['type']; ?>">
                                                <a href="<?php echo $resource['@attributes']['href']; ?>" title="<?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?>" target="_blank"><?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?></a>
                                            </div>
                                        <?php } 
                                    endforeach; ?>
                                </div>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <?php ?>
                <?php endif; ?>
                <?php $a++; 
                endif;    
            } 
                else: 
                    // if($applications['display_name'] != 'Brake & Clutch Fluid' && $applications['display_name'] != 'Power Steering' && $applications['display_name'] != 'Brake Fluid' && strpos($applications['display_name'], 'Coolant') === FALSE): //KL-72
                    if(strpos($applications['display_name'], 'Coolant') === FALSE):
            ?>
                    <?php if(!empty($applications['product'])): ?>
                        <?php $i = 0; foreach($applications['product'] as $product) { ?>
                        <tr class="row<?php echo ($a%2) ? 2 : 1 ?> prodrow<?php echo ($i%2) ? 2 : 1 ?>">
                            <?php if($i == 0){ ?>
                                <td class="application" rowspan="<?php echo count($applications['product']) ?>" style="color:#000;width:10%;padding:10px 20px;border:1px solid #000">
                                    <div><?php echo $applications['display_name']; ?></div>
                                </td>
                                <td class="tiername tiernamet1" style="color:#000;white-space:nowrap;padding:10px 20px;border:1px solid #000;width: 10%;">
                                    <div><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_BEST_TXT']; ?></div>
                                </td>
                                <td class="recommendation" style="color:#000;width:40%;padding:10px 20px;border:1px solid #000"><?php echo isset($product['name']) ? $product['name'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_NO_PRODUCT_TXT']; ?></td>
                                <td class="capacity" rowspan="<?php echo count($applications['product']) ?>" style="color:#000;padding:10px 20px;border:1px solid #000">
                                    <div><?php echo isset($applications['display_capacity']) ? $applications['display_capacity'] : ''; ?></div>
                                </td>
                                <td class="datasheet" style="color:#000;padding:10px 20px;border:1px solid #000">
                                    <?php if(isset($product['resource']) && !empty(($product['resource']))): ?>
                                        <div class="links">
                                            <?php foreach($product['resource'] as $resource): 
                                                    if(isset($resource['@attributes']) && $resource['@attributes']['type'] != 'packshot'){
                                                    ?>
                                                    <div class="datasheet datasheet-<?php echo $resource['@attributes']['type']; ?>">
                                                        <a href="<?php echo $resource['@attributes']['href']; ?>" title="<?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?>" target="_blank"><?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?></a>
                                                    </div>
                                                <?php } 
                                            endforeach; ?>
                                        </div>
                                    <?php endif; ?>
                                </td>
                            <?php } else { ?>
                                <td class="tiername tiernamet2" style="color:#000;white-space:nowrap;padding:10px 20px;border:1px solid #000;width: 10%;">
                                    <div>
                                    <?php 
                                        if(count($applications['product']) > 2) {
                                            if($i == 1) {
                                                echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_BETTER_TXT'];
                                            } else {
                                                echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_GOOD_TXT'];
                                            }
                                        } else {
                                            echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_GOOD_TXT'];
                                        } 
                                    ?>
                                    </div>
                                </td>
                                <td class="recommendation" style="color:#000;width:40%;padding:10px 20px;border:1px solid #000"><?php echo isset($product['name']) ? $product['name'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_NO_PRODUCT_TXT']; ?></td>
                                <td class="datasheet" style="color:#000;padding:10px 20px;border:1px solid #000">
                                    <?php if(isset($product['resource']) && !empty(($product['resource']))): ?>
                                        <div class="links">
                                            <?php foreach($product['resource'] as $resource): 
                                                    if(isset($resource['@attributes']) && $resource['@attributes']['type'] != 'packshot'){
                                                    ?>
                                                    <div class="datasheet datasheet-<?php echo $resource['@attributes']['type']; ?>">
                                                        <a href="<?php echo $resource['@attributes']['href']; ?>" title="<?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?>" target="_blank"><?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?></a>
                                                    </div>
                                                <?php } 
                                            endforeach; ?>
                                        </div>
                                    <?php endif; ?>
                                </td>
                            <?php } ?>
                        </tr>
                        <?php $i++; } ?>
                    <?php else: ?>
                        <tr class="row<?php echo ($a%2) ? 2 : 1 ?> prodrow1">
                            <td class="application" rowspan="1" style="color:#000;width:10%;padding:10px 20px;border:1px solid #000">
                                <div><?php echo $applications['display_name']; ?></div>
                            </td>
                            <td class="recommendation" colspan="2" style="color:#000;white-space:nowrap;padding:10px 20px;border:1px solid #000"><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_NO_PRODUCT_TXT']; ?></td>
                            <td class="capacity" rowspan="1" style="color:#000;white-space:nowrap;padding:10px 20px;border:1px solid #000">
                                <div><?php echo isset($applications['display_capacity']) ? $applications['display_capacity'] : ''; ?></div>
                            </td>
                            <td class="datasheet" style="color:#000;padding:10px 20px;border:1px solid #000">
                                <?php if(isset($applications['product']['resource']) && !empty(($applications['product']['resource']))): ?>
                                    <div class="links">
                                        <?php foreach($applications['product']['resource'] as $resource): 
                                                if(isset($resource['@attributes']) && $resource['@attributes']['type'] != 'packshot'){
                                                ?>
                                                <div class="datasheet datasheet-<?php echo $resource['@attributes']['type']; ?>">
                                                    <a href="<?php echo $resource['@attributes']['href']; ?>" title="<?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?>" target="_blank"><?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?></a>
                                                </div>
                                            <?php } 
                                        endforeach; ?>
                                    </div>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php ?>
                    <?php endif; ?>
                <?php endif;
                endif;
            }
                ?>
            </tbody>
        </table>
    <?php $email_body = ob_get_clean();
    //echo $email_body;exit;
        //$admin_email = get_option('admin_email');
        $admin_email = 'info@klondikelubricants.com';
        // To send HTML mail, the Content-type header must be set
        $headers[] = 'MIME-Version: 1.0' . "\r\n";
        $headers[] = 'Content-type: text/html; charset=UTF-8' . "\r\n";

        // Additional headers
        $headers[] = "Reply-To:" . $admin_email . "\r\n";
        $headers[] = 'From: KLONDIKE Lubricants <'. $admin_email .'>' . "\r\n";
        if(!empty($bcc)){
            $headers[] = 'Bcc: '. $bcc . "\r\n";
        }

        $attachments = array();
        if(wp_mail( $email, $title, $email_body, $headers, $attachments )) {
        //if(mail($email,$title,$email_body, implode('',$headers))) {
            echo '<p>'. $LANGUAGE['OS_EMAIL_SENT'] .'</p>';
        } else {
            echo '<p>'. $LANGUAGE['OS_EMAIL_NOT_SENT'] .'</p>';
        }
    // } else {
    //         die(__( 'Security check', 'lube-link' )); 
    //     }
    } else { ?>
    <form method="post" id="send-mail" accept-charset="UTF-8" action="<?php //echo esc_url( admin_url('admin-post.php') ); ?>">
        <p><?php echo $LANGUAGE['OS_EMAIL_FORM_PLEASE_VERIFY_TEXT'] ?></p>

        <div class="google-recaptcha">
            <div class="g-recaptcha" data-sitekey="6Lc0Y-UUAAAAAKLvMsn3FC8BrUZ2sKsNFGR_ZhmX"></div>
            <div class="error-container"></div>
        </div>
        <br/>
        <div class="emailButton">
            <div class="control-group">
                <div class="control-label"><label for="email"><?php echo $LANGUAGE['OS_EMAIL_FORM_TO_TEXT'] ?></label></div>
                <div class="controls"><input id="email" class="text" name="email" type="email" size="30" required="true" />
                    <div class="error-container"></div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="control-group">
                <div class="control-label"><label for="title"><?php echo $LANGUAGE['OS_EMAIL_FORM_SUBJECT_TEXT'] ?></label></div>
                <div class="controls"><input id="title" class="text" name="title" type="text" value="<?php echo $LANGUAGE['OS_EMAIL_FORM_SUBJECT_INPUT_TEXT'] ?>" size="30" required="true" />
                    <div class="error-container"></div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="control-group">
                <div class="control-label"><label for="bcc"><?php echo $LANGUAGE['OS_EMAIL_FORM_BCC_TEXT'] ?></label></div>
                <div class="controls"><input id="bcc" class="text" name="bcc" type="email" size="30" />
                    <div class="error-container"></div>
                </div>
                <div class="clear"></div>
            </div>
            <?php wp_nonce_field( 'oil_search_form', 'oil_search_form_nonce' ); ?>
            <input type="hidden" name="action" value="oil_search_sendemail" />
            <input type="submit" class="submit-form" value="<?php echo $LANGUAGE['OS_EMAIL_FORM_SEND_TEXT'] ?>">
        </div>
    </form>
    <script type="text/javascript">
    
    jQuery(document).ready(function(){
        jQuery("#send-mail").validate({
			ignore: [] // <-- option so that hidden elements are validated
		});
		var jForm = jQuery("form#send-mail");
		jForm.submit(function () {
            var response = grecaptcha.getResponse();

            if(response.length == 0 ||  response == '' || response ===false ) {
                jQuery('.google-recaptcha .error-container').html('<?php echo $LANGUAGE['OS_FORM_VALIDATE_G_RECAPTCHA_MSG']; ?>');
                e.preventDefault();
            } else {
                jQuery('.google-recaptcha .error-container').html('');
            }
			jForm.find("label.error").each(function () {
				var jThis = jQuery(this);
				var jContainer = jThis.closest("div.control-group");
				var jControl = jContainer.find("div.controls");
				jControl.find(".error-container").append(jThis.clone());
				jThis.remove();
			});
			var errors = jForm.find("label.error");
			if (errors.length > 0) {
				var label = jForm.find("label.error:first");
				jQuery('html, body').animate({
					'scrollTop': label.offset().top - 400
				}, 300);
			}
		});
    });
    </script>
    <?php } ?>
</div>
</div>
</body>
</html>