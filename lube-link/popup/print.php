<?php 
$equipment = getEquipmentDetail();
$lang = !empty($_GET['lang']) ? $_GET['lang'] . '/' : '';
$equipmentDetail = $equipment['equipment'];
//echo '<pre>';print_r($equipment);echo '</pre>';
?>
<div id="content">
	<div id="printButtons">
        <span id="closeButton" onclick="window.close();return false">Close</span>
        <span id="printButton" onclick="window.print();return false">Print</span>
    </div>
    <div id="printWrapper">
        <div id="printLogo"><img src="<?php echo os_base_url(); ?>assets/images/LUBE-LINK-logo-DARK.png" /></div>
        <div class="primaryRecommendationPackshot">
            <?php $resources = $equipment['equipment']['application'][0]['product'][0]; 
                if(!empty($resources['resource'])):
                    foreach($resources['resource'] as $resource) {
                        $attribute = isset($resource['@attributes']) ? $resource['@attributes'] : '';
                        if(!empty($attribute) && $attribute['type'] == 'packshot') { ?>
                        <img src="<?php echo $attribute['href'] ?>" alt="<?php echo $resources['name'] ?>" />
                    <?php }
                    }
                endif; ?>
            </div>
            <div class="primaryProductTitle">
                <h1><?php echo $equipmentDetail['display_name_short']; ?></h1>
            </div>
    
            <div class="modelInfo">
                <dl>
                    <dt><?php echo $LANGUAGE['OS_DETAIL_MAKE_TITLE'] ?></dt>
                    <dd><?php echo $equipmentDetail['manufacturer']; ?></dd>
                    <dt><?php echo $LANGUAGE['OS_DETAIL_MODEL_TITLE'] ?></dt>
                    <dd><?php echo $equipmentDetail['model']; ?></dd>
                    <dt><?php echo $LANGUAGE['OS_DETAIL_ENGINE_TITLE'] ?></dt>
                    <dd><?php echo $equipmentDetail['alt_fueltype']; ?></dd>
                    <dt><?php echo $LANGUAGE['OS_DETAIL_YEAR_TITLE'] ?></dt>
                    <dd><?php echo $equipmentDetail['display_year']; ?></dd>
                </dl>
            </div>

            <script type="text/javascript">
                $(document).ready(function(){
                    $(".recommendationTable a").click(function(){
                    var href = $(this).attr("href");
                    window.open(href, "productLinkWindow", "width=640,height=500,menubar=no,scrollbar=yes,toolbar=no,resizable=yes").focus();
                    return false;
                });
                });

            </script>

            <div class="recommendationTable">
                <table>
                    <thead>
                        <tr>
                            <th class="application"><div><?php echo $LANGUAGE['OS_POPUP_PRINT_APPLICATION_TXT']; ?></div></th>
                            <th colspan="2" class="recommendation"><div><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TXT']; ?></div></th>
                            <th class="capacity"><div><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_CAPACITY_TXT']; ?> (gal)</div></th>
                            <th class="datasheet"><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_DATASHEET_TXT']; ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $applications = $equipment['equipment']['application']; //echo '<pre>';print_r($applications);echo '</pre>';
                    if(!empty($applications)){
                        if(!isset($applications['display_name'])):
                        $a = 0; foreach($applications as $application) { 
                            // if($application['display_name'] != 'Brake & Clutch Fluid' && $application['display_name'] != 'Power Steering' && $application['display_name'] != 'Brake Fluid' && strpos($application['display_name'], 'Coolant') === FALSE): //KL-72
                            if(strpos($application['display_name'], 'Coolant') === FALSE):    
                        ?>
                        <?php if(!empty($application['product'])): ?>
                            <?php if(isset($application['product'][0])) {
                            $i = 0; foreach($application['product'] as $product) { ?>
                            <tr class="row<?php echo ($a%2) ? 2 : 1 ?> prodrow<?php echo ($i%2) ? 2 : 1 ?>">
                                <?php if($i == 0){ ?>
                                    <td class="application" rowspan="<?php echo count($application['product']) ?>">
                                        <div><?php echo $application['display_name']; ?></div>
                                    </td>
                                    <td class="tiername tiernamet1">
                                        <div><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_BEST_TXT']; ?></div>
                                    </td>
                                    <td class="recommendation"><?php echo isset($product['name']) ? $product['name'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_NO_PRODUCT_TXT']; ?></td>
                                    <td class="capacity" rowspan="<?php echo count($application['product']) ?>">
                                    <?php if(isset($application['display_capacity'])): 
                                        $capacity = (float) $application['display_capacity'];
                                    ?>
                                        <div class="capacity">
                                            <span class="recommendation"><?php echo $capacity; ?> (gal), <?php echo number_format($capacity/0.26417, 2, '.', ',') ?> (L)</span>
                                        </div>
                                    <?php endif; ?>
                                    </td>
                                    <td class="datasheet">
                                    <?php if(isset($product['resource']) && !empty(($product['resource']))): ?>
                                        <div class="links">
                                            <?php foreach($product['resource'] as $resource): 
                                                    if(isset($resource['@attributes']) && $resource['@attributes']['type'] != 'packshot'){
                                                    ?>
                                                    <div class="datasheet datasheet-<?php echo $resource['@attributes']['type']; ?>">
                                                        <a href="<?php echo $resource['@attributes']['href']; ?>" title="<?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?>" target="_blank"><?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?></a>
                                                    </div>
                                                <?php } 
                                            endforeach; ?>
                                            <?php if(isset($product['resource'][3]) && $product['resource'][3] != '') { ?>
                                            <div class="datasheet datasheet-more-information">
                                                <span data-target="#more_information_<?php echo $a . '_' . $i; ?>_3" style="color: -webkit-link;cursor: pointer;"><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MORE_INFORMATION_TXT']; ?></span>
                                            </div>
                                            <div class="more_information" id="more_information_<?php echo $a . '_' . $i; ?>_3" style="display: none;"><?php echo $product['resource'][3]; ?></div>
                                        <?php } ?>
                                        </div>
                                    <?php endif; ?>
                                    </td>
                                <?php } else { ?>
                                    <td class="tiername tiernamet2">
                                        <div>
                                        <?php 
                                            if(count($application['product']) > 2) {
                                                if($i == 1) {
                                                    echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_BETTER_TXT'];
                                                } else {
                                                    echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_GOOD_TXT'];
                                                }
                                            } else {
                                                echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_GOOD_TXT'];
                                            } 
                                        ?>
                                        </div>
                                    </td>
                                    <td class="recommendation"><?php echo isset($product['name']) ? $product['name'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_NO_PRODUCT_TXT']; ?></td>
                                    <td class="datasheet">
                                    <?php if(isset($product['resource']) && !empty(($product['resource']))): ?>
                                        <div class="links">
                                            <?php foreach($product['resource'] as $resource): 
                                                    if(isset($resource['@attributes']) && $resource['@attributes']['type'] != 'packshot'){
                                                    ?>
                                                    <div class="datasheet datasheet-<?php echo $resource['@attributes']['type']; ?>">
                                                        <a href="<?php echo $resource['@attributes']['href']; ?>" title="<?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?>" target="_blank"><?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?></a>
                                                    </div>
                                                <?php } 
                                            endforeach; ?>
                                            <?php if(isset($product['resource'][3]) && $product['resource'][3] != '') { ?>
                                                <div class="datasheet datasheet-more-information">
                                                    <span data-target="#more_information_<?php echo $a . '_' . $i; ?>_3" style="color: -webkit-link;cursor: pointer;"><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MORE_INFORMATION_TXT']; ?></span>
                                                </div>
                                                <div class="more_information" id="more_information_<?php echo $a . '_' . $i; ?>_3" style="display: none;"><?php echo $product['resource'][3]; ?></div>
                                            <?php } ?>
                                        </div>
                                    <?php endif; ?>
                                <?php } ?>
                            </tr>
                            <?php $i++; } ?>
                    <?php } elseif(isset($application['product']['name'])) { 
                        $i = 0; $product = $application['product'];
                        //echo '<pre>';print_r($application['product']);echo '</pre>';
                    ?>
                        <tr class="row<?php echo ($a%2) ? 2 : 1 ?> prodrow<?php echo ($i%2) ? 2 : 1 ?>">
                            <td class="application" rowspan="">
                                <div><?php echo $application['display_name']; ?></div>
                            </td>
                            <td class="tiername tiernamet1">
                                <div><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_BEST_TXT']; ?></div>
                            </td>
                            <td class="recommendation"><?php echo isset($product['name']) ? $product['name'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_NO_PRODUCT_TXT']; ?></td>
                            <td class="capacity" rowspan="<?php //echo count($application['product']) ?>">
                            <?php if(isset($application['display_capacity'])): 
                                $capacity = (float) $application['display_capacity'];
                            ?>
                                <div class="capacity">
                                    <span class="recommendation"><?php echo $capacity; ?> (gal), <?php echo number_format($capacity/0.26417, 2, '.', ',') ?> (L)</span>
                                </div>
                            <?php endif; ?>
                            </td>
                            <td class="datasheet">
                                <?php if(isset($product['resource']) && !empty(($product['resource']))): ?>
                                    <div class="links">
                                        <?php foreach($product['resource'] as $resource): 
                                                if(isset($resource['@attributes']) && $resource['@attributes']['type'] != 'packshot'){
                                                ?>
                                                <div class="datasheet datasheet-<?php echo $resource['@attributes']['type']; ?>">
                                                    <a href="<?php echo $resource['@attributes']['href']; ?>" title="<?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?>" target="_blank"><?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?></a>
                                                </div>
                                            <?php } 
                                        endforeach; ?>
                                        <?php if(isset($product['resource'][3]) && $product['resource'][3] != '') { ?>
                                            <div class="datasheet datasheet-more-information">
                                                <span data-target="#more_information_<?php echo $a . '_' . $i; ?>_3" style="color: -webkit-link;cursor: pointer;"><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MORE_INFORMATION_TXT']; ?></span>
                                            </div>
                                            <div class="more_information" id="more_information_<?php echo $a . '_' . $i; ?>_3" style="display: none;"><?php echo $product['resource'][3]; ?></div>
                                        <?php } ?>
                                    </div>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php } else { ?>
                        <tr class="row<?php echo ($a%2) ? 2 : 1 ?> prodrow1">
                            <td class="application" rowspan="1">
                                <div><?php echo $application['display_name']; ?></div>
                            </td>
                            <td class="recommendation" colspan="2"><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_NO_PRODUCT_TXT']; ?></td>
                            <td class="capacity" rowspan="1">
                            <?php if(isset($application['display_capacity'])): 
                                $capacity = (float) $application['display_capacity'];
                            ?>
                                <div class="capacity">
                                    <span class="recommendation"><?php echo $capacity; ?> (gal), <?php echo number_format($capacity/0.26417, 2, '.', ',') ?> (L)</span>
                                </div>
                            <?php endif; ?>
                            </td>
                            <td class="datasheet"></td>
                        </tr>
                    <?php } ?>
                        <?php else: ?>
                            <tr class="row<?php echo ($a%2) ? 2 : 1 ?> prodrow1">
                                <td class="application" rowspan="1">
                                    <div><?php echo $application['display_name']; ?></div>
                                </td>
                                <td class="recommendation" colspan="2"><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_NO_PRODUCT_TXT']; ?></td>
                                <td class="capacity" rowspan="1">
                                <?php if(isset($application['display_capacity'])): 
                                    $capacity = (float) $application['display_capacity'];
                                ?>
                                    <div class="capacity">
                                        <span class="recommendation"><?php echo $capacity; ?> (gal), <?php echo number_format($capacity/0.26417, 2, '.', ',') ?> (L)</span>
                                    </div>
                                <?php endif; ?>
                                </td>
                                <td class="datasheet">
                                    <?php if(isset($product['resource']) && !empty(($product['resource']))): ?>
                                        <div class="links">
                                            <?php foreach($product['resource'] as $resource): 
                                                    if(isset($resource['@attributes']) && $resource['@attributes']['type'] != 'packshot'){
                                                    ?>
                                                    <div class="datasheet datasheet-<?php echo $resource['@attributes']['type']; ?>">
                                                        <a href="<?php echo $resource['@attributes']['href']; ?>" title="<?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?>" target="_blank"><?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?></a>
                                                    </div>
                                                <?php } 
                                            endforeach; ?>
                                            <?php if(isset($product['resource'][3]) && $product['resource'][3] != '') { ?>
                                                <div class="datasheet datasheet-more-information">
                                                    <span data-target="#more_information_<?php echo $a . '_' . $i; ?>_3" style="color: -webkit-link;cursor: pointer;"><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MORE_INFORMATION_TXT']; ?></span>
                                                </div>
                                                <div class="more_information" id="more_information_<?php echo $a . '_' . $i; ?>_3" style="display: none;"><?php echo $product['resource'][3]; ?></div>
                                            <?php } ?>
                                        </div>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php ?>
                        <?php endif; ?>
                        <?php $a++; 
                            endif;
                        } 
                        else: 
                            // if($applications['display_name'] != 'Brake & Clutch Fluid' && $applications['display_name'] != 'Power Steering' && $applications['display_name'] != 'Brake Fluid' && strpos($applications['display_name'], 'Coolant') === FALSE): //KL-72
                            if(strpos($applications['display_name'], 'Coolant') === FALSE):
                        ?>
                            <?php if(!empty($applications['product'])): ?>
                                <?php $i = 0; foreach($applications['product'] as $product) { ?>
                                <tr class="row<?php echo ($a%2) ? 2 : 1 ?> prodrow<?php echo ($i%2) ? 2 : 1 ?>">
                                    <?php if($i == 0){ ?>
                                        <td class="application" rowspan="<?php echo count($applications['product']) ?>">
                                            <div><?php echo $applications['display_name']; ?></div>
                                        </td>
                                        <td class="tiername tiernamet1">
                                            <div><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_BEST_TXT']; ?></div>
                                        </td>
                                        <td class="recommendation"><?php echo isset($product['name']) ? $product['name'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_NO_PRODUCT_TXT']; ?></td>
                                        <td class="capacity" rowspan="<?php echo count($applications['product']) ?>">
                                            <?php if(isset($applications['display_capacity'])): 
                                                $capacity = (float) $applications['display_capacity'];
                                            ?>
                                                <div class="capacity">
                                                    <span class="recommendation"><?php echo $capacity; ?> (gal), <?php echo number_format($capacity/0.26417, 2, '.', ',') ?> (L)</span>
                                                </div>
                                            <?php endif; ?>
                                        </td>
                                        <td class="datasheet">
                                        <?php if(isset($product['resource']) && !empty(($product['resource']))): ?>
                                            <div class="links">
                                                <?php foreach($product['resource'] as $resource): 
                                                        if(isset($resource['@attributes']) && $resource['@attributes']['type'] != 'packshot'){
                                                        ?>
                                                        <div class="datasheet datasheet-<?php echo $resource['@attributes']['type']; ?>">
                                                            <a href="<?php echo $resource['@attributes']['href']; ?>" title="<?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?>" target="_blank"><?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?></a>
                                                        </div>
                                                    <?php } 
                                                endforeach; ?>
                                                <?php if(isset($product['resource'][3]) && $product['resource'][3] != '') { ?>
                                                    <div class="datasheet datasheet-more-information">
                                                        <span data-target="#more_information_<?php echo $a . '_' . $i; ?>_3" style="color: -webkit-link;cursor: pointer;"><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MORE_INFORMATION_TXT']; ?></span>
                                                    </div>
                                                    <div class="more_information" id="more_information_<?php echo $a . '_' . $i; ?>_3" style="display: none;"><?php echo $product['resource'][3]; ?></div>
                                                <?php } ?>
                                            </div>
                                        <?php endif; ?>
                                    </td>
                                    <?php } else { ?>
                                        <td class="tiername tiernamet2">
                                            <div>
                                            <?php 
                                                if(count($applications['product']) > 2) {
                                                    if($i == 1) {
                                                        echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_BETTER_TXT'];
                                                    } else {
                                                        echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_GOOD_TXT'];
                                                    }
                                                } else {
                                                    echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_GOOD_TXT'];
                                                } 
                                            ?>
                                            </div>
                                        </td>
                                        <td class="recommendation"><?php echo isset($product['name']) ? $product['name'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_NO_PRODUCT_TXT']; ?></td>
                                        <td class="datasheet">
                                            <?php if(isset($product['resource']) && !empty(($product['resource']))): ?>
                                                <div class="links">
                                                    <?php foreach($product['resource'] as $resource): 
                                                            if(isset($resource['@attributes']) && $resource['@attributes']['type'] != 'packshot'){
                                                            ?>
                                                            <div class="datasheet datasheet-<?php echo $resource['@attributes']['type']; ?>">
                                                                <a href="<?php echo $resource['@attributes']['href']; ?>" title="<?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?>" target="_blank"><?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?></a>
                                                            </div>
                                                        <?php } 
                                                    endforeach; ?>
                                                    <?php if(isset($product['resource'][3]) && $product['resource'][3] != '') { ?>
                                                        <div class="datasheet datasheet-more-information">
                                                            <span data-target="#more_information_<?php echo $a . '_' . $i; ?>_3" style="color: -webkit-link;cursor: pointer;"><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MORE_INFORMATION_TXT']; ?></span>
                                                        </div>
                                                        <div class="more_information" id="more_information_<?php echo $a . '_' . $i; ?>_3" style="display: none;"><?php echo $product['resource'][3]; ?></div>
                                                    <?php } ?>
                                                </div>
                                            <?php endif; ?>
                                        </td>
                                    <?php } ?>
                                </tr>
                                <?php $i++; } ?>
                            <?php else: ?>
                                <tr class="row<?php echo ($a%2) ? 2 : 1 ?> prodrow1">
                                    <td class="application" rowspan="1">
                                        <div><?php echo $applications['display_name']; ?></div>
                                    </td>
                                    <td class="recommendation" colspan="2"><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_NO_PRODUCT_TXT']; ?></td>
                                    <td class="capacity" rowspan="1">
                                        <?php if(isset($applications['display_capacity'])): 
                                            $capacity = (float) $applications['display_capacity'];
                                        ?>
                                            <div class="capacity">
                                                <span class="recommendation"><?php echo $capacity; ?> (gal), <?php echo number_format($capacity/0.26417, 2, '.', ',') ?> (L)</span>
                                            </div>
                                        <?php endif; ?>
                                    </td>
                                    <td class="datasheet">
                                        <?php if(isset($applications['product']['resource']) && !empty(($applications['product']['resource']))): ?>
                                            <div class="links">
                                                <?php foreach($applications['product']['resource'] as $resource): 
                                                        if(isset($resource['@attributes']) && $resource['@attributes']['type'] != 'packshot'){
                                                        ?>
                                                        <div class="datasheet datasheet-<?php echo $resource['@attributes']['type']; ?>">
                                                            <a href="<?php echo $resource['@attributes']['href']; ?>" title="<?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?>" target="_blank"><?php echo $resource['@attributes']['type'] == 'tds' ? $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT']; ?></a>
                                                        </div>
                                                    <?php } 
                                                endforeach; ?>
                                                <?php if(isset($applications['product']['resource'][3]) && $applications['product']['resource'][3] != '') { ?>
                                                    <div class="datasheet datasheet-more-information">
                                                        <span data-target="#more_information_<?php echo $a . '_' . $i; ?>_3" style="color: -webkit-link;cursor: pointer;"><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MORE_INFORMATION_TXT']; ?></span>
                                                    </div>
                                                    <div class="more_information" id="more_information_<?php echo $a . '_' . $i; ?>_3" style="display: none;"><?php echo $applications['product']['resource'][3]; ?></div>
                                                <?php } ?>
                                            </div>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php ?>
                            <?php endif; ?>
                        <?php endif;
                        endif;
                    }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="lubricantCapacityNotes">
                <h4><?php echo $LANGUAGE['OS_PRINT_CAPACITY_NOTES_TXT']; ?></h4>
                <dl>
                    <dt>a.</dt>
                    <dd><?php echo $LANGUAGE['OS_PRINT_ALTERNATIVE_RECOMMENDATIONS_TXT']; ?>: <?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_NO_PRODUCT_TXT']; ?></dd>
                </dl>
            </div>
        </div>
	</div>
</div>
        </div>
        <script type="text/javascript">
            $(document).ready( function() {
                $('.datasheet-more-information span').click(function(e){
                    e.preventDefault();
                    var dest = $(this).attr('data-target');
                    $(dest).toggle();
                });
            });
        </script>
    </body>
</html>