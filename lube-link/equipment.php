<?php //echo '<pre>';print_r($_GET);echo '</pre>';
    if(isset($_GET['print']) && $_GET['print'] == 1) {
        include ('header-popup.php'); 
        include ('popup/print.php'); 
    } elseif(isset($_GET['email_form']) && $_GET['email_form'] == 1) { 
        include ('header-popup.php'); 
        include ('popup/email_form.php'); 
    } else {
?>
<?php include ('header.php'); 

    $equipment = getEquipmentDetail();
    $lang = !empty($_GET['lang']) ? $_GET['lang'] . '/' : '';
?>
<div id="recommendationPage">
    <?php if(!empty($equipment)): ?>
    <?php $equipmentDetail = $equipment['equipment']; //echo '<pre>';print_r($equipment);echo '</pre>'; ?>
    <div class="actionTop">
        <ul>
        <li id="printButton">
                <script type="text/javascript">
                function showPrintWindow() {

                    window.open("<?php echo getCurrentUrl(); ?>/print/", "PrintWindow", "width=640,height=500,menubar=no,scrollbars=1,toolbar=no,resizable=yes").focus();
                    return false;
                }
                </script>

                <a href="<?php echo getCurrentUrl(); ?>/print/" title="Print" onclick="return showPrintWindow();" class="printPage"><i class="fa fa-print" aria-hidden="true"></i><span style="margin-left: 5px;"><?php echo $LANGUAGE['OS_BTN_PRINT_TXT']; ?></span></a>
            </li>
            <li id="emailButton">
                <script type="text/javascript">
                function showEmailWindow() {

                    window.open("<?php echo getCurrentUrl(); ?>/email_form/", "EmailWindow", "width=640,height=500,menubar=no,scrollbar=yes,toolbar=no,resizable=yes").focus();
                    return false;
                }
                </script>

                    <a href="<?php echo getCurrentUrl(); ?>/email_form/" title="Email" onclick="return showEmailWindow();" class="emailPage"><i class="fa fa-envelope-o" aria-hidden="true"></i><span style="margin-left: 5px;"><?php echo $LANGUAGE['OS_BTN_EMAIL_TXT']; ?></span></a>
            </li>
            
            <li id="searchButton"><a href="<?php echo os_base_url(true) . $lang; ?>" class="returnToSearchPage"><i class="fa fa-search" aria-hidden="true"></i><span style="margin-left: 5px;"><?php echo $LANGUAGE['OS_BTN_NEW_SEARCH_TXT']; ?></span></a></li>
        </ul>
    </div>
    <div id="modelRecommendation">
        <script type="text/javascript">
        //   $(document).ready(function(){
        //     if ( $(".block:first").find(".coptions").length ) {
        //       $(".primaryProductTitle").hide();
        //     }
        //   });
        </script>
        <!--div class="equipment-right">
            <?php //$resources = $equipment['equipment']['application'][0]['product'][0]; 
            //if(!empty($resources['resource'])):
                //foreach($resources['resource'] as $resource) {
                    //$attribute = isset($resource['@attributes']) ? $resource['@attributes'] : '';
                    //if(!empty($attribute) && $attribute['type'] == 'packshot') { ?>
                    <img src="<?php //echo $attribute['href'] ?>" alt="<?php //echo $resources['name'] ?>" />
                   <?php //}
                //}
            //endif; ?>
        </div-->
        <div class="equipment-left">
            <h1 class="primaryProductTitle"><?php echo $equipmentDetail['display_name_short']; ?></h1>
            <!--p class="is-recommended"><?php //echo $LANGUAGE['OS_DETAIL_RECOMMENDED_TITLE'] ?></p-->       
            <div class="modelInfo">
                <ul>
                    <li>
                        <p>
                            <strong><?php echo $LANGUAGE['OS_DETAIL_MAKE_TITLE'] ?></strong>
                            <span><?php echo $equipmentDetail['manufacturer']; ?></span>
                        </p>
                    </li>
                    <li>
                        <p>
                            <strong><?php echo $LANGUAGE['OS_DETAIL_ENGINE_TITLE'] ?></strong>
                            <span><?php echo $equipmentDetail['alt_fueltype']; ?></span>
                        </p>
                    </li>
                    <li>
                        <p>
                            <strong><?php echo $LANGUAGE['OS_DETAIL_MODEL_TITLE'] ?></strong>
                            <span><?php echo $equipmentDetail['model']; ?></span>
                        </p>
                    </li>
                    <li>
                        <p>
                            <strong><?php echo $LANGUAGE['OS_DETAIL_YEAR_TITLE'] ?></strong>
                            <span><?php echo $equipmentDetail['display_year']; ?></span>
                        </p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="clearfix"></div>
        <!--div class="equipment-collapsible-actions">
            <ul>
                <li><button type="button" onclick="accordion_expand_all()"><?php //echo $LANGUAGE['OS_EXPAND_ALL_TXT'] ?> <i class="fa fa-arrow-circle-down" aria-hidden="true"></i></button></li>
                <li><button type="button" onclick="accordion_collapse_all()"><?php //echo $LANGUAGE['OS_COLLAPSE_ALL_TXT'] ?> <i class="fa fa-times-circle" aria-hidden="true"></i></button></li>
            </ul>
        </div-->
        <div id="accordion">
            <?php $applications = $equipment['equipment']['application']; //echo '<pre>';print_r($applications);echo '</pre>';
            if(!empty($applications)){
                if(!isset($applications['display_name'])):
                $a = 0; foreach($applications as $application) { 
                    // if($application['display_name'] != 'Brake & Clutch Fluid' && $application['display_name'] != 'Power Steering' && $application['display_name'] != 'Brake Fluid' && strpos($application['display_name'], 'Coolant') === FALSE): //KL-72
                    if(strpos($application['display_name'], 'Coolant') === FALSE):
                ?>
                    <h3><?php echo $application['display_name']; ?></h3>
                    <div>
                        <div class="header"><h4><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TXT'] ?></h4></div>
                        <?php if(!empty($application['product'])): ?>
                            <?php //echo '<pre>';print_r($application['product']);echo '</pre>'; ?>
                        <ul class="products">
                        <?php if(isset($application['product'][0])) {
                            $i = 0; foreach($application['product'] as $product) { ?>
                            <li><?php //echo isset($product['resource']) ? 'exist' : 'not exist'; ?>
                                <?php if(isset($product['resource']) && !empty(($product['resource']))): ?>
                                <div class="product-thumb">
                                <?php foreach($product['resource'] as $resource): 
                                        if(isset($resource['@attributes']) && $resource['@attributes']['type'] == 'packshot'){
                                        ?>
                                        <img src="<?php echo $resource['@attributes']['href'] ?>" alt="<?php echo $product['name']; ?>" />
                                    <?php } 
                                    endforeach; ?>
                                </div>
                                <?php endif; ?>
                                <div class="recommendations">
                                    <h5 class="product-title"><span class="tiername">
                                    <?php 
                                    if ($i == 0) {
                                        echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_BEST_TXT'];
                                    } else {
                                        if(count($application['product']) > 2) {
                                            if($i == 1) {
                                                echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_BETTER_TXT'];
                                            } else {
                                                echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_GOOD_TXT'];
                                            }
                                        } else {
                                            echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_GOOD_TXT'];
                                        } 
                                        
                                    } ?></span>
                                    <span class="recommendation"><?php echo isset($product['resource'][5]) ? $product['resource'][5] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_NO_PRODUCT_TXT']; ?></span>
                                    </h5>
    
                                    <?php if(isset($product['resource']) && !empty(($product['resource']))): ?>
                                    <div class="links">
                                        <?php foreach($product['resource'] as $resource): 
                                                if(isset($resource['@attributes']) && $resource['@attributes']['type'] != 'packshot' && $resource['@attributes']['type'] != 'product_description' && $resource['@attributes']['type'] != 'product_title'){
                                                ?>
                                                <div class="datasheet datasheet-<?php echo $resource['@attributes']['type']; ?>">
                                                    <a href="<?php echo $resource['@attributes']['href']; ?>" title="<?php echo $resource['@attributes']['type']; ?>" target="_blank">
                                                        <?php
                                                        if($resource['@attributes']['type'] == 'tds'){
                                                            echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'];
                                                        }
                                                        elseif($resource['@attributes']['type'] == 'msds'){
                                                            echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT'];
                                                        }
                                                        else{
                                                            echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MORE_INFORMATION_TXT'];
                                                        }
                                                        ?>
                                                    </a>
                                                        
                                                </div>
                                            <?php } 
                                        endforeach; ?>
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <div class="clearfix"></div>
                            </li>
                            <?php $i++; } ?>
                        <?php } elseif(isset($application['product']['name'])) { ?>
                            <li><?php $i = 0; $product = $application['product']; ?>
                                <?php if(isset($product['resource']) && !empty(($product['resource']))): ?>
                                <div class="product-thumb">
                                <?php foreach($product['resource'] as $resource): 
                                        if(isset($resource['@attributes']) && $resource['@attributes']['type'] == 'packshot'){
                                        ?>
                                        <img src="<?php echo $resource['@attributes']['href'] ?>" alt="<?php echo $product['name']; ?>" />
                                    <?php } 
                                    endforeach; ?>
                                </div>
                                <?php endif; ?>
                                <div class="recommendations">
                                    <h5 class="product-title"><span class="tiername">
                                    <?php 
                                    if ($i == 0) {
                                        echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_BEST_TXT'];
                                    } else {
                                        if(count($application['product']) > 2) {
                                            if($i == 1) {
                                                echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_BETTER_TXT'];
                                            } else {
                                                echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_GOOD_TXT'];
                                            }
                                        } else {
                                            echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_GOOD_TXT'];
                                        } 
                                        
                                    } ?></span>
                                    <span class="recommendation">
                                        <?php echo isset($product['resource'][5]) ? $product['resource'][5] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_NO_PRODUCT_TXT']; ?>
                                    </span>
                                    </h5>
    
                                    <?php if(isset($product['resource']) && !empty(($product['resource']))): ?>
                                    <div class="links">
                                        <?php foreach($product['resource'] as $resource): 
                                                if(isset($resource['@attributes']) && $resource['@attributes']['type'] != 'packshot' && $resource['@attributes']['type'] != 'product_description' && $resource['@attributes']['type'] != 'product_title'){
                                                ?>
                                                <div class="datasheet datasheet-<?php echo $resource['@attributes']['type']; ?>">
                                                    <a href="<?php echo $resource['@attributes']['href']; ?>" title="<?php echo $resource['@attributes']['type']; ?>" target="_blank">
                                                        <?php
                                                        if($resource['@attributes']['type'] == 'tds'){
                                                            echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'];
                                                        }
                                                        elseif($resource['@attributes']['type'] == 'msds'){
                                                            echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT'];
                                                        }
                                                        else{
                                                            echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MORE_INFORMATION_TXT'];
                                                        }
                                                        ?>
                                                    </a>
                                                </div>
                                            <?php } 
                                        endforeach; ?>
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <div class="clearfix"></div>
                            </li>
                        <?php } else { ?>
                            <li>
                                <div class="recommendations">
                                    <h5 class="product-title"><span class="tiername">
                                    <?php 
                                    if ($i == 0) {
                                        echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_BEST_TXT'];
                                    } else {
                                        if(count($application['product']) > 2) {
                                            if($i == 1) {
                                                echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_BETTER_TXT'];
                                            } else {
                                                echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_GOOD_TXT'];
                                            }
                                        } else {
                                            echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_GOOD_TXT'];
                                        } 
                                        
                                    } ?></span>
                                    <span class="recommendation"><?php echo isset($product['resource'][5]) ? $product['resource'][5] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_NO_PRODUCT_TXT']; ?></span>
                                    </h5>
                                </div>
                            </li>
                        <?php } ?>
                        </ul>
                        <?php else: ?>
                            <ul class="products">
                                <li>
                                    <div class="recommendations">
                                        <h5 class="product-title"><span class="tiername"><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_BEST_TXT'] ?></span><span class="recommendation"><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_NO_PRODUCT_TXT']; ?></span></h5>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                            </ul>
                        <?php endif; ?>
                        <?php if(isset($application['display_capacity'])): 
                            $capacity = (float) $application['display_capacity'];
                        ?>
                        <div class="clearfix"></div>
                        <div class="capacity">
                            <span class="tiername"><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_CAPACITY_TXT']; ?></span>
                            <span class="recommendation"><?php echo $capacity; ?> (gal), <?php echo number_format($capacity/0.26417, 2, '.', ',') ?> (L)</span>
                        </div>
                        <?php endif; ?>
                    </div>
                <?php $a++; 
                        endif;
                    } 
                else: 
                    // if($applications['display_name'] != 'Brake & Clutch Fluid' && $applications['display_name'] != 'Power Steering' && $applications['display_name'] != 'Brake Fluid' && strpos($applications['display_name'], 'Coolant') === FALSE): //KL-72
                    if(strpos($applications['display_name'], 'Coolant') === FALSE):
                ?>
                    <h3><?php echo $applications['display_name']; ?></h3>
                    <div>
                        <div class="header"><h4><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TXT'] ?></h4></div>
                        <?php if(!empty($applications['product'])): ?>
                            <?php //echo '<pre>';print_r($application['product']);echo '</pre>'; ?>
                        <ul class="products">
                            <?php $i = 0; foreach($applications['product'] as $product) { ?>
                            <li>
                                <?php if(isset($product['resource']) && !empty(($product['resource']))): ?>
                                <div class="product-thumb">
                                <?php foreach($product['resource'] as $resource): 
                                        if(isset($resource['@attributes']) && $resource['@attributes']['type'] == 'packshot'){
                                        ?>
                                        <img src="<?php echo $resource['@attributes']['href'] ?>" alt="<?php echo $product['name']; ?>" />
                                    <?php } 
                                    endforeach; ?>
                                </div>
                                <?php endif; ?>
                                <div class="recommendations">
                                    <h5 class="product-title"><span class="tiername">
                                    <?php if ($i == 0) {
                                        echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_BEST_TXT'];
                                    } else {
                                        if(count($applications['product']) > 2) {
                                            if($i == 1) {
                                                echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_BETTER_TXT'];
                                            } else {
                                                echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_GOOD_TXT'];
                                            }
                                        } else {
                                            echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_GOOD_TXT'];
                                        }
                                        
                                    } ?></span>
                                    <span class="recommendation"><?php echo isset($product['resource'][5]) ? $product['resource'][5] : $LANGUAGE['OS_APPLICATION_RECOMMENDATION_NO_PRODUCT_TXT']; ?></span>
                                    </h5>
    
                                    <?php if(isset($product['resource']) && !empty(($product['resource']))): ?>
                                    <div class="links">
                                        <?php foreach($product['resource'] as $resource): 
                                                if(isset($resource['@attributes']) && $resource['@attributes']['type'] != 'packshot' && $resource['@attributes']['type'] != 'product_description' && $resource['@attributes']['type'] != 'product_title'){
                                                ?>
                                                <div class="datasheet datasheet-<?php echo $resource['@attributes']['type']; ?>">
                                                    <a href="<?php echo $resource['@attributes']['href']; ?>" title="<?php echo $resource['@attributes']['type']; ?>" target="_blank">
                                                        <?php
                                                        if($resource['@attributes']['type'] == 'tds'){
                                                            echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'];
                                                        }
                                                        elseif($resource['@attributes']['type'] == 'msds'){
                                                            echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT'];
                                                        }
                                                        else{
                                                            echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_MORE_INFORMATION_TXT'];
                                                        }
                                                        ?>
                                                    </a>
                                                </div>
                                            <?php } 
                                        endforeach; ?>
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <div class="clearfix"></div>
                            </li>
                            <?php $i++; } ?>
                        </ul>
                        <?php endif; ?>
                        <?php if(isset($applications['display_capacity'])): 
                            $capacity = (float) $applications['display_capacity'];
                        ?>
                        <div class="capacity">
                            <span class="tiername"><?php echo $LANGUAGE['OS_APPLICATION_RECOMMENDATION_CAPACITY_TXT']; ?></span>
                            <span class="recommendation"><?php echo $capacity; ?> (gal), <?php echo number_format($capacity/0.26417, 2, '.', ',') ?> (L)</span>
                        </div>
                        <?php endif; ?>
                    </div>
                <?php endif; 
                endif;
            } ?>
        </div>
        <?php //echo '<pre>';print_r($resources);echo '</pre>'; ?>
        <div class="equipment-notice"><?php echo $LANGUAGE['OS_EQUIPMENT_NOTICE_TXT']; ?></div>
    </div>
    <div class="actionTop fixBottom">
        <ul>
        <li id="printButton">
                <script type="text/javascript">
                function showPrintWindow() {

                    window.open("<?php echo getCurrentUrl(); ?>/print/", "PrintWindow", "width=640,height=500,menubar=no,scrollbars=1,toolbar=no,resizable=yes").focus();
                    return false;
                }
                </script>

                <a href="<?php echo getCurrentUrl(); ?>/print/" title="Print" onclick="return showPrintWindow();" class="printPage"><i class="fa fa-print" aria-hidden="true"></i><span style="margin-left: 5px;"><?php echo $LANGUAGE['OS_BTN_PRINT_TXT']; ?></span></a>
            </li>
            <li id="emailButton">
                <script type="text/javascript">
                function showEmailWindow() {

                    window.open("<?php echo getCurrentUrl(); ?>/email_form/", "EmailWindow", "width=640,height=500,menubar=no,scrollbar=yes,toolbar=no,resizable=yes").focus();
                    return false;
                }
                </script>

                    <a href="<?php echo getCurrentUrl(); ?>/email_form/" title="Email" onclick="return showEmailWindow();" class="emailPage"><i class="fa fa-envelope-o" aria-hidden="true"></i><span style="margin-left: 5px;"><?php echo $LANGUAGE['OS_BTN_EMAIL_TXT']; ?></span></a>
            </li>
            
            <li id="searchButton"><a href="<?php echo os_base_url(true) . $lang; ?>" class="returnToSearchPage"><i class="fa fa-search" aria-hidden="true"></i><span style="margin-left: 5px;"><?php echo $LANGUAGE['OS_BTN_NEW_SEARCH_TXT']; ?></span></a></li>
        </ul>
    </div>
    <?php else: ?>
    <div class="dialog">
        <h1><?php echo $LANGUAGE['OS_DIALOG_DOESNOT_EXIST_TITLE'] ?></h1>
        <p><?php echo $LANGUAGE['OS_DIALOG_DOESNOT_EXIST_DESC'] ?></p>
    </div>
    <?php endif; ?>

    <script type="text/javascript">
        $(document).ready( function() {
            $('.datasheet-more-information a').click(function(e){
                e.preventDefault();
                var dest = $(this).attr('href');
                $(dest).toggle();
            });
            var icons = {
                header: "equipment_ui_da",
                activeHeader: "equipment_ui_a"
            };
            if($('#accordion').length){
                $('#accordion').accordion({
                    icons: icons,
                    heightStyle: "content",
                    collapsible: true,
                    beforeActivate: function(event, ui) {
                        // The accordion believes a panel is being opened
                        if (ui.newHeader[0]) {
                            var currHeader  = ui.newHeader;
                            var currContent = currHeader.next('.ui-accordion-content');
                        // The accordion believes a panel is being closed
                        } else {
                            var currHeader  = ui.oldHeader;
                            var currContent = currHeader.next('.ui-accordion-content');
                        }
                        // Since we've changed the default behavior, this detects the actual status
                        var isPanelSelected = currHeader.attr('aria-selected') == 'true';
                        // Toggle the panel's header
                        currHeader.toggleClass('ui-corner-all',isPanelSelected).toggleClass('ui-accordion-header-active ui-state-active ui-corner-top',!isPanelSelected).attr('aria-selected',((!isPanelSelected).toString()));
                        // Toggle the panel's icon
                        currHeader.children('.ui-icon').toggleClass('equipment_ui_da',isPanelSelected).toggleClass('equipment_ui_a',!isPanelSelected);
                        // Toggle the panel's content
                        currContent.toggleClass('ui-accordion-content-active',!isPanelSelected)    
                        if (isPanelSelected) { currContent.slideUp(); }  else { currContent.slideDown(); }
                        return false; // Cancels the default action
                    }
                });
            }
        });
        setTimeout(function(){
            accordion_expand_all();
        },500);
        function accordion_expand_all()
        {
        var sections = $('#accordion').find("h3");
        sections.each(function(index, section){
            if ($(section).hasClass('ui-state-default') && !$(section).hasClass('ui-accordion-header-active')) {
                $(section).click();
            }
        });

        }

        function accordion_collapse_all()
        {
            var sections = $('#accordion').find("h3");
            sections.each(function(index, section){
                if ($(section).hasClass('ui-state-active')) {
                    $(section).click();
                }
            });
        }
    </script>
</div>
<?php include ('footer.php'); 
}