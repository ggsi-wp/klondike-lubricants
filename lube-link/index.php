<?php include ('header-home.php'); ?>
    <div id="facetSearch">
    
        <script type="text/javascript">
        $(document).ready(function(){
            $("#modelSearchButton input").prop('disabled', true);
        
            $("#modelSearchInput input").keyup(function(){
                var length = $(this).val().length;
                if ( length > 0) {
                $("#modelSearchButton input").prop('disabled', false);
                } else {
                $("#modelSearchButton input").prop('disabled', true);
                }
            });
        });
        </script>

        <h2 class="os-title"><?php esc_html_e( $LANGUAGE['OS_TITLE_TEXT'], 'lube-link' ); ?></h2>

        <form name="keyword_search" method="get" action="<?php echo os_base_url(); ?><?php echo isset($_GET['lang']) ? $_GET['lang'] . '/search/' : 'search/'; ?>">
        <div class="modelSearchContainer facetDropdown">
        <?php $browse = getBrowseList(); //echo '<pre>';print_r($browse);echo '</pre>'; ?>
            <div id="modelSearchInput" class="modelSearchBlock os-home-search-text">
                <input class="text" name="q" type="text" size="30" value="" placeholder="<?php esc_html_e( $LANGUAGE['OS_INPUT_PLACEHOLDER_TEXT'], 'lube-link' ); ?>" />
            </div>
            <div id="modelSearchSelect" class="modelSearchBlock os-home-search-select os-select">
                <select id="familygroup" name="familygroup">
                    <option selected="" value=""><?php esc_html_e( $LANGUAGE['OS_INPUT_BROWSE_OPTION_ALL'], 'lube-link' ); ?></option>
                    <?php if(!empty($browse) && isset($browse['response']['index']['item'])) {
                        foreach($browse['response']['index']['item'] as $item) { ?>
                         <option value="<?php echo $item; ?>"><?php echo $item; ?></option>
                    <?php }
                    } ?>
                    <!--option value="Motorcycles"><?php esc_html_e( $LANGUAGE['OS_INPUT_BROWSE_OPTION_MOTORCYCLES'], 'lube-link' ); ?></option>
                    <option value="Cars"><?php esc_html_e( $LANGUAGE['OS_INPUT_BROWSE_OPTION_CARS'], 'lube-link' ); ?></option>
                    <option value="Vans"><?php esc_html_e( $LANGUAGE['OS_INPUT_BROWSE_OPTION_VANS'], 'lube-link' ); ?></option>
                    <option value="Commercial"><?php esc_html_e( $LANGUAGE['OS_INPUT_BROWSE_OPTION_COMMERCIAL'], 'lube-link' ); ?></option>
                    <option value="Agricultural"><?php esc_html_e( $LANGUAGE['OS_INPUT_BROWSE_OPTION_AGRICULTURAL'], 'lube-link' ); ?></option>
                    <option value="Off-Highway"><?php esc_html_e( $LANGUAGE['OS_INPUT_BROWSE_OPTION_OFF_HIGHWAY'], 'lube-link' ); ?></option>
                    <option value="Industrial"><?php esc_html_e( $LANGUAGE['OS_INPUT_BROWSE_OPTION_INDUSTRIAL'], 'lube-link' ); ?></option-->
                </select>
            </div>
            <div id="modelSearchButton" class="modelSearchBlock os-home-search-btn">
                <input class="fa button" type="submit" onmouseover="this.className='fa button buttonhover'" onmouseout="this.className='fa button'" value="" disabled="">
            </div>
        </div>
        </form>
        <div class="clearfix"></div>
    </div>
    <script type="text/javascript">
        // jQuery(document).ready(function($) {
        //     fixBottom();
        //     $(window).resize(function(){
        //         if($(window).width() <= 767) {
        //             fixBottom();
        //         }
        //     });
        // });
        // function fixBottom() {
        //     var link = jQuery('#facetSearch');
            
        //     var offset = link.offset();
        //     var top = offset.top;
        //     var left = offset.left;
        //     var bottom = jQuery(window).height() - top - link.height() + 30;
        //     //bottom = bottom - offset.top; // or bottom -= offset.top;
        //     link.css({'padding-bottom': bottom + 'px'});
        // }
    </script>
    <script>
    $(document).ready(function() {
        $('#facetSearchBG') .css({'height': (($(window).height() + 75) )});
        $(window).bind('resize', function(){
            $('#facetSearchBG').css({'height': (($(window).height() + 75) )});
        });
    });

    </script>
    
<?php include ('footer.php'); ?>   