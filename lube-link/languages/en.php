<?php $LANGUAGE = [];
$LANGUAGE['OS_PAGE_TITLE'] = 'KLONDIKE | LUBE-LINK Oil Search Tool';
$LANGUAGE['OS_SEARCH_PAGE_TITLE'] = 'Refine Results';
$LANGUAGE['OS_LANG_TEXT_EN'] = 'English (US)';
$LANGUAGE['OS_LANG_TEXT_FR'] = 'French';
$LANGUAGE['OS_TITLE_TEXT'] = 'FIND THE RIGHT OIL FOR YOUR APPLICATION';
$LANGUAGE['OS_INPUT_PLACEHOLDER_TEXT'] = 'Enter make & model';
$LANGUAGE['OS_INPUT_BROWSE_OPTION_ALL'] = 'All';
$LANGUAGE['OS_INPUT_BROWSE_OPTION_MOTORCYCLES'] = 'Motorcycles';
$LANGUAGE['OS_INPUT_BROWSE_OPTION_CARS'] = 'Cars';
$LANGUAGE['OS_INPUT_BROWSE_OPTION_VANS'] = 'Vans';
$LANGUAGE['OS_INPUT_BROWSE_OPTION_COMMERCIAL'] = 'Commercial';
$LANGUAGE['OS_INPUT_BROWSE_OPTION_AGRICULTURAL'] = 'Agricultural';
$LANGUAGE['OS_INPUT_BROWSE_OPTION_OFF_HIGHWAY'] = 'Off-Highway';
$LANGUAGE['OS_INPUT_BROWSE_OPTION_INDUSTRIAL'] = 'Industrial';
$LANGUAGE['OS_RESULT_SHOWING_TXT'] = 'Showing';
$LANGUAGE['OS_RESULT_OF_TXT'] = 'of';
$LANGUAGE['OS_RESULT_RESULTS_TXT'] = 'results';
$LANGUAGE['OS_RESULT_BTN_PREV_TXT'] = 'Prev';
$LANGUAGE['OS_RESULT_BTN_NEXT_TXT'] = 'Next';
$LANGUAGE['OS_RESULT_EMPTY_TXT'] = 'Sorry, no results matched your search.';
$LANGUAGE['OS_RESULT_NOT_VEHICLE_TXT'] = 'Don\'t see your vehicle';
$LANGUAGE['OS_BTN_CLEAR_FILTER_TXT'] = 'Clear Filters';
$LANGUAGE['OS_BTN_NEW_SEARCH_TXT'] = 'New Search';
$LANGUAGE['OS_FILTER_FACET_TYPE_FUELTYPE'] = 'Fuel Type';
$LANGUAGE['OS_FILTER_FACET_TYPE_YEAR'] = 'Within Year Range';
$LANGUAGE['OS_FILTER_FACET_TYPE_YEAR_CLEAR'] = 'Year';
$LANGUAGE['OS_FILTER_FACET_TYPE_SERIES'] = 'Series';
$LANGUAGE['OS_FILTER_FACET_TYPE_DISPLACEMENT'] = 'Engine Size';
$LANGUAGE['OS_FILTER_FACET_TYPE_MANUFACTURER'] = 'Manufacturer';
$LANGUAGE['OS_FILTER_FACET_TYPE_FAMILYGROUP'] = 'Family group';
$LANGUAGE['OS_FILTER_FACET_TYPE_FAMILY'] = 'Family';
$LANGUAGE['OS_FILTERED_BY_TXT'] = 'Filtered By';
$LANGUAGE['OS_FILTER_RESULTS_TABLE_HEAD_CATEGORY'] = 'Category';
$LANGUAGE['OS_FILTER_RESULTS_TABLE_HEAD_MANUFACTURER'] = 'Manufacturer';
$LANGUAGE['OS_FILTER_RESULTS_TABLE_HEAD_MODEL'] = 'Model';
$LANGUAGE['OS_FILTER_RESULTS_TABLE_HEAD_YEAR'] = 'Year';
$LANGUAGE['OS_FILTER_RESULTS_TABLE_HEAD_FUEL'] = 'Fuel';
//Detail page
$LANGUAGE['OS_DETAIL_RECOMMENDED_TITLE'] = 'Is recommended for your';
$LANGUAGE['OS_DETAIL_MAKE_TITLE'] = 'Make: ';
$LANGUAGE['OS_DETAIL_MODEL_TITLE'] = 'Model: ';
$LANGUAGE['OS_DETAIL_ENGINE_TITLE'] = 'Engine: ';
$LANGUAGE['OS_DETAIL_YEAR_TITLE'] = 'Year: ';
$LANGUAGE['OS_DIALOG_DOESNOT_EXIST_TITLE'] = 'The page you were looking for doesn\'t exist.';
$LANGUAGE['OS_DIALOG_DOESNOT_EXIST_DESC'] = 'You may have mistyped the address or the page may have moved.';
$LANGUAGE['OS_BTN_PRINT_TXT'] = 'Print Page';
$LANGUAGE['OS_BTN_EMAIL_TXT'] = 'Email Page';
$LANGUAGE['OS_EXPAND_ALL_TXT'] = "Expand All";
$LANGUAGE['OS_COLLAPSE_ALL_TXT'] = 'Close All';
$LANGUAGE['OS_APPLICATION_RECOMMENDATION_TXT'] = 'Recommendation: ';
$LANGUAGE['OS_APPLICATION_RECOMMENDATION_BEST_TXT'] = 'Best: ';
$LANGUAGE['OS_APPLICATION_RECOMMENDATION_BETTER_TXT'] = 'Better: ';
$LANGUAGE['OS_APPLICATION_RECOMMENDATION_GOOD_TXT'] = 'Good: ';
$LANGUAGE['OS_APPLICATION_RECOMMENDATION_TDS_TXT'] = 'PDS';
$LANGUAGE['OS_APPLICATION_RECOMMENDATION_MSDS_TXT'] = 'SDS';
$LANGUAGE['OS_APPLICATION_RECOMMENDATION_CAPACITY_TXT'] = 'Capacity: ';
$LANGUAGE['OS_APPLICATION_RECOMMENDATION_NO_PRODUCT_TXT'] = 'Please <a href="/contact-klondike/">contact us</a> for fluid recommendations.';
$LANGUAGE['OS_APPLICATION_RECOMMENDATION_MORE_INFORMATION_TXT'] = 'More Information';
$LANGUAGE['OS_EQUIPMENT_NOTICE_TXT'] = 'Disclaimer: <br/>Always follow OEM recommendations for fluid viscosity and API service category. KLONDIKE takes no responsibility for product misuse or misapplication.';
//Print popup
$LANGUAGE['OS_POPUP_PRINT_APPLICATION_TXT'] = 'Application';
$LANGUAGE['OS_PRINT_CAPACITY_NOTES_TXT'] = 'Lubricant / Capacity Notes';
$LANGUAGE['OS_PRINT_ALTERNATIVE_RECOMMENDATIONS_TXT'] = 'Alternative recommendations';
$LANGUAGE['OS_APPLICATION_RECOMMENDATION_DATASHEET_TXT'] = 'Data Sheet';
//Email form
$LANGUAGE['OS_EMAIL_FORM_PLEASE_VERIFY_TEXT'] = 'Please verify that you are not a robot.';
$LANGUAGE['OS_EMAIL_FORM_SEND_TEXT'] = 'Send';
$LANGUAGE['OS_EMAIL_FORM_TO_TEXT'] = 'To';
$LANGUAGE['OS_EMAIL_FORM_SUBJECT_TEXT'] = 'Subject (optional)';
$LANGUAGE['OS_EMAIL_FORM_SUBJECT_INPUT_TEXT'] = 'LUBE-LINK by KLONDIKE - Lubricant Recommendations Report';
$LANGUAGE['OS_EMAIL_FORM_BCC_TEXT'] = 'Bcc';
$LANGUAGE['OS_FORM_VALIDATE_EMAIL_MSG'] = 'Please enter valid email address.';
$LANGUAGE['OS_FORM_VALIDATE_TITLE_MSG'] = 'Please enter a title';
$LANGUAGE['OS_FORM_VALIDATE_G_RECAPTCHA_MSG'] = 'Please verify if you not a robot';
$LANGUAGE['OS_EMAIL_BODY_RECOMMENDATION_FOR_MSG'] = 'Recommendation For';
$LANGUAGE['OS_EMAIL_SENT'] = 'Your email has been sent successfully.';
$LANGUAGE['OS_EMAIL_NOT_SENT'] = 'Email not sent';
$LANGUAGE['OS_FOOTER_CONTACT_HEADING'] = 'CONTACT US';