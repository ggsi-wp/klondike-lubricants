<?php 
require_once('../wp-config.php');
/** Root folder */
define( 'OS_BASE_PART', '/lube-link/' );

/** API Feed name */
define( 'OS_FEED_NAME', 'klondike-ws-canada' );

/** API Security Token */
define( 'OS_SECURITY_TOKEN', 'LiEoiv0tqygb' );

/** Browse List */
define( 'OS_BROWSE_LINK', 'http://klondike-ws-canada.phoenix.earlweb.net/browse/' );

/** Search Link */
define( 'OS_SEARCH_LINK', 'http://klondike-ws-canada.phoenix.earlweb.net/search/' );

/** Equipment Link */
define( 'OS_EQUIPMENT_LINK', 'http://klondike-ws-canada.phoenix.earlweb.net/' );


if(!function_exists('os_base_url')) {
    function os_base_url($part = true) {
        $urlParth = ($part) ? '/' : '';
        $url = site_url( '/lube-link'. $urlParth );
        // Display the link 
        return $url; 
    }
}

function getSearchResuts() {
    $results = array();
    $searchQuery = getCurrentQueryUrl();
    
    if(!empty($searchQuery)){
        $language = !empty($_GET['lang']) ? '&language=' . $_GET['lang'] : '';
        $api_url = OS_SEARCH_LINK . '?token=' . OS_SECURITY_TOKEN . '&' .$searchQuery . $language;
        $buffer = file_get_contents($api_url);
        $xml   = simplexml_load_string($buffer);
        $results = json_encode($xml);
        $results = json_decode($results, TRUE);
    }
    return $results;
}

function getBrowseList() {
    $language = !empty($_GET['lang']) ? '&language=' . $_GET['lang'] : '';
    $api_url = OS_BROWSE_LINK . '?token=' . OS_SECURITY_TOKEN . $language;

    $buffer = file_get_contents($api_url);
    $xml   = simplexml_load_string($buffer);
    $browse = XML2Array($xml);
    $browse = array($xml->getName() => $browse);

    return $browse;
}

function XML2Array(SimpleXMLElement $parent)
{
    $array = array();

    foreach ($parent as $name => $element) {
        ($node = & $array[$name])
            && (1 === count($node) ? $node = array($node) : 1)
            && $node = & $node[];

        $node = $element->count() ? XML2Array($element) : trim($element);
    }

    return $array;
}

function getCurrentUrl() {
    return $_SERVER['REQUEST_URI'];
}

function getCurrentQueryUrl(){
    $url = getCurrentUrl();
    $parse_url = wp_parse_url($url);
    return $parse_url['query'];
}

function getCurrentLanguage() {
    $currentLang = '';
    if(!empty($_GET['lang'])) {
        $currentLang = $_GET['lang'];
    } else {
        $currentLang = 'en';
    }
    return $currentLang;
}

function getEquipmentDetail() {
    $equipment = array();
    $currentLang = getCurrentLanguage();
    $curentUrl = str_replace($currentLang . '/', '', str_replace(OS_BASE_PART, '', getCurrentUrl()));
    $curentUrl = str_replace('/print/', '', $curentUrl);
    $curentUrl = str_replace('/email_form/', '', $curentUrl);
    if(!empty($curentUrl)){
        $language = !empty($_GET['lang']) && $_GET['lang'] == 'fr' ? '&language=' . $_GET['lang'] : '&language=en_US';
        $api_url = OS_EQUIPMENT_LINK . $curentUrl .'?token=' . OS_SECURITY_TOKEN . $language;
        //echo $api_url;
        $buffer = file_get_contents($api_url);
        $xml   = simplexml_load_string($buffer);
        $equipment = json_encode($xml);
        $equipment = json_decode($equipment, TRUE);
    }
    return $equipment;
}

function attachInlineImage() {  
    global $phpmailer;  
    $file = ABSPATH . '/lube-link/assets/images/LUBE-LINK-logo-DARK.jpg'; //phpmailer will load this file  
    $uid = 'klondike-logo-uid'; //will map it to this UID  
    $name = 'LUBE-LINK-logo-DARK.jpg'; //this will be the file name for the attachment  
    if (is_file($file)) {  
      $phpmailer->AddEmbeddedImage($file, $uid, $name);  
    }  
  }  
  
  add_action('phpmailer_init','attachInlineImage'); 