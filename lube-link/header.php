<?php require_once('functions.php'); ?>
<?php 
$language = !empty($_GET['lang']) ? $_GET['lang'] . '.php' : 'en.php';
include('languages/' . $language);
$queryLang = !empty($_GET['lang']) ? $_GET['lang'] . '/' : '';
$curentUrl = str_replace($queryLang, '', str_replace(OS_BASE_PART, '', getCurrentUrl()));
?>
<html>
    <head>
        <title><?php esc_html_e( $LANGUAGE['OS_PAGE_TITLE'], 'lube-link' ); ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" type="image/x-icon">

        <link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-57x57.png">

        <link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-60x60.png">

        <link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-72x72.png">

        <link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-76x76.png">

        <link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-114x114.png">

        <link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-120x120.png">

        <link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-144x144.png">

        <link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-152x152.png">

        <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-180x180.png">

        <link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo('stylesheet_directory'); ?>/android-icon-192x192.png">

        <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('stylesheet_directory'); ?>/images/favicon-32x32.png">

        <link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo('stylesheet_directory'); ?>/images/favicon-96x96.png">

        <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('stylesheet_directory'); ?>/images/favicon-16x16.png">

        <link rel="manifest" href="/manifest.json">

        <meta name="msapplication-TileColor" content="#ffffff">

        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">

        <meta name="theme-color" content="#ffffff">

        <link rel="icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" type="image/x-icon">

        <link href="<?php echo os_base_url(); ?>assets/css/font-awesome-4.7.0.css" rel="stylesheet">
        <link href="<?php echo os_base_url(); ?>assets/css/fontello.css" rel="stylesheet">
        <link href="<?php echo os_base_url(); ?>assets/css/style.css" rel="stylesheet">
        <script src="<?php echo os_base_url(); ?>assets/js/applications.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PD7RKC4');</script>
<!-- End Google Tag Manager -->

        
    </head>
    <body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PD7RKC4"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    
    <div id="canvas">
            <div id="facetResultsBlock" class="page-container">
                <div class="header-container">
                    <div class="header-logo">
                        <a href="<?php echo get_site_url('https'); ?>/lube-link/" title="<?php esc_html_e( $LANGUAGE['OS_PAGE_TITLE'], 'lube-link' ); ?>"><img src="<?php echo os_base_url(); ?>assets/images/LUBE-LINK-logo.png" /></a>
                    </div>
                    <div class="language-container">
                        <div id="languageSelector">
                            <script type="text/javascript">
                                $(document).ready(function(){
                                    $("body").on('change', 'select.language', function(){
                                        window.location.href=$(this).val();
                                    });   
                                });
                            </script>
                            <div class="languageSelector os-select">
                                <?php $currentQuery = getCurrentQueryUrl(); //echo getCurrentUrl(); ?>
                                <select class="language">
                                    <option value="<?php echo os_base_url(true); ?>en/<?php echo $curentUrl; ?>" <?php echo (isset($_GET['lang']) && $_GET['lang'] != 'fr') ? 'selected' : ''; ?>><?php esc_html_e( $LANGUAGE['OS_LANG_TEXT_EN'], 'lube-link' ); ?></option>
                                    <option value="<?php echo os_base_url(true); ?>fr/<?php echo $curentUrl; ?>" <?php echo (isset($_GET['lang']) && $_GET['lang'] == 'fr') ? 'selected' : ''; ?> ><?php esc_html_e( $LANGUAGE['OS_LANG_TEXT_FR'], 'lube-link' ); ?></option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <?php //echo '<pre>';print_r($_SERVER);echo '</pre>'; ?>