<?php /*
------------ ATTENTION ------------
If you need to edit this template, do not edit the version in the plugin directory. Place a copy in your template folder and edit it there.
This will allow you to upgrade FoxyShop without breaking your customizations. More details here: http://www.foxy-shop.com/documentation/theme-customization/
-----------------------------------
*/ ?>
<?php get_header(); ?>
<?php foxyshop_include('header'); ?>
<div id="foxyshop_container">
	<?php
	while (have_posts()) : the_post();

		$product_categories = get_the_terms("", "foxyshop_categories");
		$product_class = "";
		foreach($product_categories as $cat) {
			$product_class.=$cat->slug." ";
		}

		//Initialize Product
		global $product;
		$product = foxyshop_setup_product();
		//This is for testing to see what is included in the $product array
		//print_r($product);
		//Initialize Form
		foxyshop_start_form();
		//Write Breadcrumbs
		//foxyshop_breadcrumbs(" &raquo; ", "&laquo; Back to Products");
		//Shows Main Image and Optional Slideshow
		//Available Built-in Options: prettyPhoto (lightbox), cloud-zoom (inline zooming), or colorbox (native FoxyCart lightbox)
		//Second arg writes css and js includes on page
		//If you want to make more customizations, you can grab the code from helperfunctions.php line ~650 and paste here
		//-------------------------------------------------------------------------------------------------------------------------
		echo '<span class='.$product_class.'</span>';
		foxyshop_build_image_slideshow("prettyPhoto", true);
		echo '</span>';
		//foxyshop_build_image_slideshow("cloud-zoom", true); //Note, make sure to use jQuery 1.7.2 as 1.8+ seems to be incompatible for now
		//foxyshop_build_image_slideshow("colorbox", true); //only recommended for 0.7.2+
		//Main Product Information Area
		echo '<div class="foxyshop_product_info">';
			//edit_post_link('<img src="' . FOXYSHOP_DIR . '/images/editicon.png" alt="Edit Product" width="16" height="16" />','<span class="foxyshop_edit_product">','</span>');
			echo '<h2>' . apply_filters('the_title', $product['name']) . '</h2>';
			//Show a sale tag if the product is on sale
			//if (foxyshop_is_on_sale()) echo '<p class="sale-product">SALE!</p>';
			//Product Is New Tag (number of days since added)
			//if (foxyshop_is_product_new(14)) echo '<p class="new-product">NEW!</p>';
			
			//Show Variations (showQuantity: 0 = Do Not Show Qty, 1 = Show Before Variations, 2 = Show Below Variations)
			//If Qty is turned off on product, Qty box will not be shown at all
			//foxyshop_product_variations(2);
			//(style) clear floats before the submit button
			echo '<div class="clr"></div>';
			//Check Inventory Levels and Display Status (last variable allows backordering of out of stock items)
			//foxyshop_inventory_management("There are only %c item%s left in stock.", "Item is not in stock.", false);
			//Add On Products ($qty [1 or 0], $before_entry, $after_entry)
			// foxyshop_addon_products();
			//Add To Cart Button
			// echo '<button type="submit" name="x:productsubmit" id="productsubmit" class="foxyshop_button">Add To Cart</button>';
			//Shows the Price (includes sale price if applicable)
			// echo '<div id="foxyshop_main_price">';
				// foxyshop_price();
			// echo '</div>';
			//Shows any related products
			//foxyshop_related_products("Related Products");
			//Custom Code Can Go Here
		?>
		<?php if( have_rows('product_info_acf') ): ?>
		<table>
			<tr>
				<th>Size</th>
				<th>Part Number</th>
				<th>Units/Case</th>
			</tr>
			<?php while( have_rows('product_info_acf') ): the_row();
				// vars
				$name = get_sub_field('size');
				$part_number = get_sub_field('part_number');
				$quantity = get_sub_field('quantity/case');
			?>
			<tr>
				<td><?php echo $name; ?></td>
				<td><?php echo $part_number; ?></td>
				<td><?php echo $quantity; ?></td>
			</tr>
			<?php endwhile; ?>
		</table>
		<?php endif; ?>
		<?php if( have_rows('technical_information_acf') ): ?>
		<span>Technical Information</span>
		<ul>
			<?php while( have_rows('technical_information_acf') ): the_row();
				// vars
				$name = get_sub_field('name');
				$link = get_sub_field('link');
			?>
			<li>
				<?php if( $link ): ?>
				<a href="<?php echo $link; ?>" target="_blank">
					<?php endif; ?>
					<?php echo $name; ?>
					<?php if( $link ): ?>
				</a>
				<?php endif; ?>
			</li>
			<?php endwhile; ?>
		</ul>
		<?php endif; ?>
		<?php
			//Ends the form
		echo '</div>';
		//Main Product Description
		
	echo '</form>';
	echo $product['description'];
endwhile;
?>
<?php if( have_rows('brands') ): ?>
<ul class="brands">
	<?php while( have_rows('brands') ): the_row();
		// vars
		$image = get_sub_field('image');
		$brandLink = get_sub_field('brandslink');
	?>
	<li class="brand">
		<a href="<?php echo $brandLink; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" /></a>
	</li>
	<?php endwhile; ?>
</ul>
<?php endif; ?>
<div class="clr"></div>
</div>
<?php get_footer(); ?>
<?php /*?><!-- Product list for navigation -->
<?php
	//get the product categories
	$args = array('hide_empty' => 0, 'hierarchical' => 0, 'parent' => $categoryID, 'orderby' => 'id', 'order' => 'ASC');
	$termchildren = get_terms('foxyshop_categories', apply_filters('foxyshop_category_children_query',$args));
	
	// echo '<ul class="foxyshop_category_list">';

	foreach( $termchildren as $termchild ) {
		$currentCategorySlug = $termchild->slug;

		

		//Run the query for all products in this category
		$args = array('post_type' => 'foxyshop_product', "foxyshop_categories" => $currentCategorySlug, 'post_status' => 'publish', 'posts_per_page' => foxyshop_products_per_page(), 'paged' => get_query_var('paged'));
		$args = array_merge($args,foxyshop_sort_order_array());
		$args = array_merge($args,foxyshop_hide_children_array($currentCategoryID));
		query_posts($args);
echo '<ul class="menu FoxyShop_Show_Categories_widget">';
	foxyshop_simple_category_children(0, -1);
echo '</ul>';
		echo '<ul class="foxyshop_product_list product-list-nav">';
	
			while (have_posts()) :
				the_post();
				//Product Display
				foxyshop_include('product-nav');
			endwhile;
		echo '</ul>';
		echo '</li>';
	}
// echo '</ul>';
?><?php ?>
<?php foxyshop_include('footer'); ?>
<?php get_footer(); ?>