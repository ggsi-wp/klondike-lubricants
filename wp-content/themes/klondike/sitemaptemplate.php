<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
/*
Template Name: Sitemap Template
*/

get_header(); ?>


		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="post" id="post-<?php the_ID(); ?>">
		<h1><?php the_title(); ?></h1>
			<div class="entry">
				<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
                <ul id="sitemap">
                <?php wp_list_pages('title_li=&exclude='); ?>
				</ul>

			</div>
		</div>
		<?php endwhile; endif; ?>
	<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>


<?php get_footer(); ?>
