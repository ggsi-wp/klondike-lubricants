<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
/*
Template Name: Overview Template
*/

get_header(); ?>

<div class="entry-content">
	<h1><?php the_title(); ?></h1>
	<?php the_content(); ?>
	<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
	<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
</div><!-- .entry-content -->

<div class="section-overview">



    <?php 
		$page_overviews = get_field('page_overview');
		$count = 0;
		// $even = (++$j % 2 == 0) ? 'evenpost' : 'oddpost';
		if ($page_overviews) : ?>
			
			<ul class="cf">
                <?php 
				foreach ($page_overviews as $page_overview) :

					echo '<li class="so-item '.(++$count%2 ? "odd" : "even").'">';
						echo '<a class="title" href="'.$page_overview['link'].'">'.$page_overview['title'].'</a>';
						echo '<p>'.$page_overview['content'].'</p>';
						echo '<a class="more" href="'.$page_overview['link'].'">Discover More</a>';
						echo '<div class="img-holder"><div><a href="'.$page_overview['link'].'"></a><img src="'.$page_overview['image']['url'].'" /></div></div>';
					echo '</li>';
					
				endforeach;
				?>
			</ul>
			
	<?php endif;?>
</div>


<?php get_footer(); ?>
