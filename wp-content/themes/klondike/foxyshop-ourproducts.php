<?php 

/*

Template Name: Foxy Our Products Template

*/

?>



<?php get_header(); ?>



<?php foxyshop_include('header'); ?>





<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<div class="post" id="post-<?php the_ID(); ?>">

		<div class="entry">

			<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>

		</div>

	</div>

	<?php endwhile; endif; ?>

<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>







<div id="foxyshop_container" class="products-container">

	<?php



	//echo '<h1 id="foxyshop_category_title">Products</h1>';



	//Show all children that have a parent of 0 (top level ones)

	//Options: (Parent ID) (Show Product Count in Parentheses) <- Shows all child products (including sub categories)

	foxyshop_category_children(13, false);



	?>

</div>

<?php foxyshop_include('footer'); ?>



<script type="text/javascript">

jQuery(document).ready(function($){

	//This is set up for a three-column display. For a two column you need to do: nth-child(odd)

	$(".foxyshop_categories>li:nth-child(4n+1)").addClass("newrow");

	$(".foxyshop_categories").addClass("cf");

});

</script>



<?php get_footer(); ?>