<?php
/**
* The Header for our theme.
*
* Displays all of the <head> section and everything up till <div class="c-w1">
	* <?php get_search_form(); ?>
	*/
	?>
	
	<!doctype html>
	<!--[if lt IE 9 ]> <html class="no-js ie8" lang="en"> <![endif]-->
	<!--[if (gte IE 9)|!(IE)]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		
		<title><?php
		/*
		* Print the <title> tag based on what is being viewed.
		*/
		global $page, $paged, $post;
		
		wp_title( '|', true, 'right' );
		// Add the blog name.
		bloginfo( 'name' );
		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			echo " | $site_description";
		// Add a page number if necessary:
		if ( $paged >= 2 || $page >= 2 )
			echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );
		?></title>
		
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" type="image/x-icon">
		<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo('stylesheet_directory'); ?>/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('stylesheet_directory'); ?>/images/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo('stylesheet_directory'); ?>/images/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('stylesheet_directory'); ?>/images/favicon-16x16.png">
		<link rel="manifest" href="/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<link rel="icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" type="image/x-icon">
		<link href="<?php bloginfo('stylesheet_directory'); ?>/css/core.css" rel="stylesheet" type="text/css" media="all" />
		<link href="<?php bloginfo('stylesheet_directory'); ?>/css/main.css" rel="stylesheet" type="text/css" media="screen" />
		<link href="<?php bloginfo('stylesheet_directory'); ?>/css/skin.css" rel="stylesheet" type="text/css" media="screen" />
		<link href="<?php bloginfo('stylesheet_directory'); ?>/css/content.css" rel="stylesheet" type="text/css" media="all" />
		<link href="<?php bloginfo('stylesheet_directory'); ?>/css/print.css" rel="stylesheet" type="text/css" media="print" />
		
		
		
		<!--[if (gte IE 6)&(lte IE 8)]>
		<script type="text/javascript" src="/scripts/selectivizr-min.js"></script>
		<script src="http://s3.amazonaws.com/nwapi/nwmatcher/nwmatcher-1.2.5-min.js"></script>
		<![endif]-->
		<!-- jQuery at the bottom please -->
		<script src="<?php bloginfo('stylesheet_directory'); ?>/scripts/modernizr-2.6.2.min.js" type="text/javascript"></script>
		
		<?php
			/* We add some JavaScript to pages with the comment form
			* to support sites with threaded comments (when in use).
			*/
			if ( is_singular() && get_option( 'thread_comments' ) )
				wp_enqueue_script( 'comment-reply' );
		/* Always have wp_head() just before the closing </head>
		* tag of your theme, or you will break many plugins, which
		* generally use this hook to add elements to <head> such
			* as styles, scripts, and meta tags.
			*/
			wp_enqueue_script('jquery');
			wp_head();
		?>
		<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-50449453-1', 'klondikelubricants.com');
		ga('send', 'pageview');
		</script>
	</head>
	<body <?php body_class(); ?>>
		<div id="wrapper"><div class="w-w1"><div class="w-w2">
			
			<header id="header" class="page-section"><div class="h-w1"><div class="h-w2 section-inner">
				<nav id="mainnav" class="nav cf">
					<?php wp_nav_menu( array( 'depth' => '2', 'sort_column' => 'menu_order', 'container_class' => 'menu' ) ); ?>
					<div id="drop_products"><div class="section-wrapper cf">
						<?php
							$dropdown_products = get_field('dropdown_products', 'option');
						if ($dropdown_products) : ?>
						<ul id="dp-links" class="cf">
							<?php
														foreach ($dropdown_products as $dropdown_product) :
															echo '<li><a class="dp-'. $dropdown_product['title'] .'" href="'. $dropdown_product['link'] .'">'. $dropdown_product['title'] .'</a></li>';
														endforeach;
							?>
						</ul>
						<ul id="dp-imgs" class="cf">
							<?php
														foreach ($dropdown_products as $dropdown_product) :
															echo '<li class="dp-'. $dropdown_product['title'] .' dp-imgs-item  '. $dropdown_product['title'] .'">
																		<a href="'. $dropdown_product['link'] .'">
																		<img src="'. $dropdown_product['image']['url'] .'" alt="'. $dropdown_products['alt'] .'" /></a></li>';
															endforeach;
								?>
							</ul>
							<?php endif;?>
						</div></div>
					</nav>
					<?php
								$logo = get_field('logo', 'option');
								if ($logo) :
									echo '<a href="/" id="logo"><img src="'. $logo['url'] .'" alt="'. $logo['alt'] .'" /></a>';
					endif;
					?>
					<div id="header-banner">
						<?php
							if( is_page() || is_single() ){
								global $post;
									$parent_title = get_the_title($post->post_parent);
								echo '<span class="title">'.$parent_title.'</span>';
						}
						?>
					</div>
					<div id="util">
						<?php
							$utilities = get_field('utilities', 'option');
						if ($utilities) : ?>
						
						<ul>
							<?php
														foreach ($utilities as $util) :
															echo '<li class="' .$util['class']. '"><a href="'. $util['link'] .'">'. $util['title'] .'</a></li>';
														endforeach;
							?>
						</ul>
						
						<?php endif;?>
					</div>
					
				</div></div></header>
				
				<?php if( !is_page_template('homepage.php') ){?>
				<div id="main" class="page-section"><div class="m-w1"><div class="m-w2 section-inner cf">
					<div id="content"><div class="c-w1">
						<?php } ?>