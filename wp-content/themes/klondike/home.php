<?php







get_header(); ?>



		



		<h1>Tech/News Blog</h1>



		<div class='categories'>

			<?php dynamic_sidebar( 'primary-widget-area' ); ?>

		</div>



		<div class='pagination'>

			<?php 

				echo paginate_links(

					array(

					'prev_text'          => __('<'),

					'next_text'          => __('>'),

					)

				);

			?>

		</div>



		<?php



		if (have_posts()) : while (have_posts()) : the_post(); ?>



		<article class="post" id="post-<?php the_ID(); ?>">

			<div class="entry">

				<?php 
				
				if(get_field('blog_thumbnail'))
				{
					echo '<img src='. get_field('blog_thumbnail') .' class="attachment-thumbnail wp-post-image" />';
				}
				
				/* if ( has_post_thumbnail() ) {

					the_post_thumbnail('thumbnail');

				}*/ ?>

				<div class='inner'>



				    <div class='meta'><?php echo get_the_date('d F '); ?> <span>in</span> <?php echo get_the_category_list() ?></div>

				    <h1 class="post-title" id="post-<?php the_ID(); ?>">

						<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">

							<?php the_title(); ?>

						</a>

					</h1> 

					<?php

						the_excerpt('<p class="serif">Read the rest of this page &raquo;</p>');

						 // the_content('<p class="serif">Read the rest of this page &raquo;</p>'); 

					?>

				</div>

				<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>



			</div>

		</article>

		<?php endwhile; endif; ?>

        

	<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>











<?php get_footer(); ?>