<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>
			</div></div>

			<div id="subnav" class="nav aside">				
            	<?php  include('subnav.php');  ?>
                
                

                
                
                
				<?php if( is_archive() ){
					
					 wp_nav_menu( array( 'menu' => 'Product Sidebar', 'container_class' => 'menu-header' ) ); 
					
					//find out what sub category is in
					//$term = get_term_by('slug', get_query_var('term'), "foxyshop_categories");

					//get the parent category
				//	$currentCategoryID = $term->parent;
					//show the sub categories
            	//	foxyshop_simple_category_children($currentCategoryID, -1);
				
					// A second sidebar for widgets, just because.
	if ( is_active_sidebar( 'foxyshop-side-menu' ) ) : 
				 dynamic_sidebar( 'foxyshop-side-menu' ); 
 	endif; 
            	}
				
				
				                


				
				?>

            	<?php if( is_single() ){
					 wp_nav_menu( array( 'menu' => 'Product Sidebar', 'container_class' => 'menu-header' ) ); 
					 
					//find out what sub category is in
					//$term = wp_get_post_terms($post->ID, 'foxyshop_categories');

					//get the parent category					
				//	$currentCategoryID= $term[0]->parent;

					//$currentCategoryID = $matchedTerm->parent;
				//	foxyshop_simple_category_children($currentCategoryID, -1);
				
				if ( is_active_sidebar( 'foxyshop-side-menu' ) ) : 
				 dynamic_sidebar( 'foxyshop-side-menu' ); 
 	endif; 
				
            	}?>
			</div>

			<div id="product-family"><div class="section-wrapper">
			  <a id="product-bigger" class="hidetext" href="#">Zoom</a>
			  <?php 
					$product_image_small = get_field('product_image_small', 'option');
					if ($product_image_small) :
						
						echo '<img src="'. $product_image_small['url'] .'" alt="'. $product_image_small['alt'] .'" />';
						
					endif;
				?>
			</div></div>
			

		</div></div>

		<div id="m-background">
			<div class="mb1">
				<span></span>
				<div id="bannermask"></div>
				<?php if( is_page_template('homepage.php') ){ ?>
				<div id="homemask"></div>
				<div id="bg-slideshow">
					<ul class="rslides">
						<?php
							$bSlides = get_field('background_slide');

							if($bSlides){
								foreach($bSlides as $bSlide):
									echo '<li><img src="'.$bSlide['image']['url'].'" ></li>';
								endforeach;
							} 
						?>
					</ul>
				</div>
				<?php } ?>
			</div>				
		</div>
		</div>
		<footer id="footer" class="page-section"><div class="f-w1"><div class="f-w2 section-inner">

			<div id="actionlinks">
				<?php 
					$actionlinks = get_field('action_links', 'options');

					if($actionlinks){
					foreach($actionlinks as $actionlink):
				?>
		    	<a class="btn_cta" href="<?php echo $actionlink['link']; ?>"><span><?php echo $actionlink['title']; ?></span></a>
		    	<?php endforeach; } ?>
		    </div>

		    <span class="f-title">
		    	<div>
		    	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/maple-leaf.png" /></div>
				<?php 
					$ftitle = get_field('footer_title', 'option');
					if ($ftitle) :
						
						echo $ftitle;
						
					endif;
				?>
		    </span>
            <div id="copyright">
                <?php 
					$copyright = get_field('copyright', 'option');
					if ($copyright) :
						
						echo $copyright;
						
					endif;
				?>

				<?php 
					$footer_links = get_field('footer_links', 'option');
					if ($footer_links) : ?>
						
						<ul>
                            <?php 
							foreach ($footer_links as $footer_link) :
								echo '<li><a href="'. $footer_link['link'] .'">'. $footer_link['title'] .'</a></li>';
							endforeach;
							?>
						</ul>
						
				<?php endif;?>

				
            	<?php 
					$social_links = get_field('social_links', 'option');
					if ($social_links) : ?>
						
						<ul>
                            <?php 
							foreach ($social_links as $social_link) :
								echo '<li><a class="b-'. $social_link['title'] .'" href="'. $social_link['link'] .'" target="_blank">'. $social_link['title'] .'</a></li>';
							endforeach;
							?>
						</ul>
						
				<?php endif;?>

            </div>

		</div></div></footer>
		
	</div></div>
	<div id="product-family-lightbox" class="hidden">
		<?php 
				$product_image_large = get_field('product_image_large', 'option');
				if ($product_image_large) :
					
					echo '<img src="'. $product_image_large['url'] .'" alt="'. $product_image_large['alt'] .'" height="'. $product_image_large['height'] .'" width="'. $product_image_large['width'] .'" />';
					
				endif;
			?>
	</div>

</div>

<!-- SCRIPTS -->   

	<!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if necessary -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script>window.jQuery || document.write("<script src='<?php bloginfo('stylesheet_directory'); ?>/scripts/jquery-1.10.2.min.js'>\x3C/script>")</script>
	<?php if( is_page_template('homepage.php') ){ ?>
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/scripts/jquery.waterwheelCarousel.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/scripts/responsiveslides.min.js"></script>
	<script type="text/javascript" >
		(function($) {
			$(document).ready(function() {
				$(".rslides").responsiveSlides({
			        pager: true,
			        speed: 1000
			    });

			    $('ul.rslides_tabs').appendTo('.h-w2');

			    var carousel = $("#carousel").waterwheelCarousel({
		          separation: 258,
		          separationMultiplier: 1,
		          horizonOffsetMultiplier: 1,
		          sizeMultiplier: 1,
		          linkHandling:2 ,
		          opacityMultiplier: 0.1,
		          flankingItems: 3
		        });

		        $('.ca-prev').bind('click', function () {
		          carousel.prev();
		          return false
		        });

		        $('.ca-next').bind('click', function () {
		          carousel.next();
		          return false;
		        });
			}); 
		})(jQuery);
	</script>
	<?php } ?>
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/scripts/site-functions.js"></script>
    
	<?php wp_footer() ?>

</body>
</html>
