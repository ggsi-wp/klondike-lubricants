﻿// Functions specific to a site.
(function($) {
	$(document).ready(function() {
		// Place document manipulation code here
		$(document).on("scroll",function(){
		    $('#header').toggleClass("small", $(document).scrollTop()>100);
		});
		$('#drop_products').appendTo('#menu-item-95');
			//Dropdown link hover
		  	$('#dp-links a').hover(
		  		function(){
		  			var itemHover = '#dp-imgs li.' + $(this).attr('class').replace(/^(\S*).*/, '$1'); //get the first class
					$(itemHover).toggleClass('dp-selected');
		  		}
		  	);
		  	//Dropdown image hover
		  	$('#dp-imgs li').hover(
		  		function(){
		  			var itemHover = '#dp-links a.' + $(this).attr('class').replace(/^(\S*).*/, '$1'); //get the first class
		  			$(this).toggleClass('dp-selected');
					$(itemHover).toggleClass('hover');
		  		}
		  	);
		$('a#product-bigger').on('click', function(e){
			e.preventDefault();
			$('<div class="product-overlay" style="display:block;"></div>').appendTo('#wrapper');
			$imgHeight = $('#product-family-lightbox img').attr('height') / 2;
			$imgWidth = $('#product-family-lightbox img').attr('width') / 2;
			$('#product-family-lightbox')
				.removeClass('hidden')
				.css({
					"margin-left": -$imgWidth,
					"margin-top": -$imgHeight
				});
				
			
			$('.product-overlay, #product-family-lightbox').on('click', function(){
				$('.product-overlay').remove();
				$('#product-family-lightbox').addClass('hidden');
			});
		});
		
		if( $('body').hasClass('foxyshop') ){
			$('#header-banner').append('<span class="title2">Our Products</span>');
		}
		
		//input placeholder (use the "placeholder" attribute on your fields - this script fills in for IE)
		if(!Modernizr.input.placeholder){
			$('[placeholder]').focus(function() {
			  var input = $(this);
			  if (input.val() == input.attr('placeholder')) {
				input.val('');
				input.removeClass('placeholder');
			  }
			}).blur(function() {
			  var input = $(this);
			  if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.addClass('placeholder');
				input.val(input.attr('placeholder'));
			  }
			}).blur();
			$('[placeholder]').parents('form').submit(function() {
			  $(this).find('[placeholder]').each(function() {
				var input = $(this);
				if (input.val() == input.attr('placeholder')) {
				  input.val('');
				}
			  })
			});
		
		}
// Foxy shop sub navigation
if($('#menu-product-sidebar').length){
	var currentLocation = location;
	$('#menu-product-sidebar > li').each(function(){
			var aLink = $(this).find('> a').attr('href');
			if(currentLocation == aLink){
				//alert('sdf');
				$(this).addClass('active-parent-subnav');	
			}
		});
}

var accord = $('.accord-sec');
var allPanels = $('.accord>ul>li>ul').hide();
var aspeed = 300;
accord.on('click', function(){

	$this = $(this);
	$target = $(this).next();
	
	$this.toggleClass('on');
	
	if(!$target.hasClass('active')){
		accord.removeClass('on');
		$this.addClass('on');
		allPanels.removeClass('active').slideUp(aspeed);		
        $target.addClass('active').slideDown(aspeed);

	}else{		
		allPanels.removeClass('active').slideUp(aspeed);
		
	}
	return false;
	
});

/*if($('.foxyshop_product_list.product-list-nav').length){
		$('.cat-item.foxyshop-active-category').append($('.foxyshop_product_list.product-list-nav'));
		var pathname = window.location;
	//	alert(pathname);
		$('.foxyshop_product_list.product-list-nav li').each(function(){
				var aPath = $(this).find('a').attr('href');
				
				if (pathname == aPath)
				{
					$(this).addClass('active');
				}
				
			});
	}*/
		
//	Superfish drop-down menus
//		$('.main-nav ul.L1').superfish({
//			delay: 500, // one second delay on mouseout
//			animation: { opacity: 'show' }, // fade-in and slide-down animation
//			speed: 'fast', // faster animation speed
//			autoArrows: false,
//			dropShadows: false
//		}); 		
	}); 
})(jQuery);