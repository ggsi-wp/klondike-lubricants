<?php
/**
 * Graphically Speaking Services Inc.
 * http//:graphicallyspeaking.ca
 * version: 1.0
 */
/** Tell WordPress to run gssi_setup() when the 'after_setup_theme' hook is run. */
add_action( 'after_setup_theme', 'gssi_setup' );
if ( ! function_exists( 'gssi_setup' ) ):
function gssi_setup() {
	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();
	// This theme uses post thumbnails
	add_theme_support( 'post-thumbnails' );
	// Add default posts and comments RSS feed links to head
	add_theme_support( 'automatic-feed-links' );
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'gssi' ),
	) );
	
	// Remove Generator Meta Tag 
	remove_action('wp_head', 'wp_generator');
}
endif;

// Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
function gssi_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'gssi_page_menu_args' );
// Sets the post excerpt length to 40 characters.
function gssi_excerpt_length( $length ) {
	return 40;
}
add_filter( 'excerpt_length', 'gssi_excerpt_length' );
// Returns a "Read More" link for excerpts
function gssi_read_more_link() {
	return ' <a href="'. get_permalink() . '">' . __( 'Read More', 'gssi' ) . '</a>';
}
// Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and gssi_read_more_link().
function gssi_auto_excerpt_more( $more ) {
	return ' &hellip;' . gssi_read_more_link();
}
add_filter( 'excerpt_more', 'gssi_auto_excerpt_more' );
// Adds a pretty "Continue Reading" link to custom post excerpts.
function gssi_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= gssi_read_more_link();
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'gssi_custom_excerpt_more' );
if ( ! function_exists( 'gssi_comment' ) ) :
// Template for comments and pingbacks.
function gssi_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<div id="comment-<?php comment_ID(); ?>">
		<div class="comment-author vcard">
			<?php echo get_avatar( $comment, 40 ); ?>
			<?php printf( __( '%s <span class="says">says:</span>', 'gssi' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
		</div><!-- .comment-author .vcard -->
		<?php if ( $comment->comment_approved == '0' ) : ?>
			<em><?php _e( 'Your comment is awaiting moderation.', 'gssi' ); ?></em>
			<br />
		<?php endif; ?>
		<div class="comment-meta commentmetadata"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
			<?php
				/* translators: 1: date, 2: time */
				printf( __( '%1$s at %2$s', 'gssi' ), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)', 'gssi' ), ' ' );
			?>
		</div><!-- .comment-meta .commentmetadata -->
		<div class="comment-body"><?php comment_text(); ?></div>
		<div class="reply">
			<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		</div><!-- .reply -->
	</div><!-- #comment-##  -->
	<?php
			break;
		case 'pingback'  :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'gssi' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __('(Edit)', 'gssi'), ' ' ); ?></p>
	<?php
			break;
	endswitch;
}
endif;
// Register widgetized areas, including two sidebars
function gssi_widgets_init() {
	// Area 1, located at the top of the sidebar.
	register_sidebar( array(
		'name' => __( 'Primary Widget Area', 'gssi' ),
		'id' => 'primary-widget-area',
		'description' => __( 'The primary widget area', 'gssi' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	// Area 2, located below the Primary Widget Area in the sidebar. Empty by default.
	register_sidebar( array(
		'name' => __( 'Secondary Widget Area', 'gssi' ),
		'id' => 'secondary-widget-area',
		'description' => __( 'The secondary widget area', 'gssi' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}
// Register sidebars by running gssi_widgets_init() on the widgets_init hook. 
add_action( 'widgets_init', 'gssi_widgets_init' );
// Removes the default styles that are packaged with the Recent Comments widget.
function gssi_remove_recent_comments_style() {
	global $wp_widget_factory;
	remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
}
add_action( 'widgets_init', 'gssi_remove_recent_comments_style' );
if ( ! function_exists( 'gssi_posted_on' ) ) :
// Prints HTML with meta information for the current post—date/time and author.
function gssi_posted_on() {
	printf( __( '<span class="%1$s">Posted on</span> %2$s <span class="meta-sep">by</span> %3$s', 'gssi' ),
		'meta-prep meta-prep-author',
		sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><span class="entry-date">%3$s</span></a>',
			get_permalink(),
			esc_attr( get_the_time() ),
			get_the_date()
		),
		sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s">%3$s</a></span>',
			get_author_posts_url( get_the_author_meta( 'ID' ) ),
			sprintf( esc_attr__( 'View all posts by %s', 'gssi' ), get_the_author() ),
			get_the_author()
		)
	);
}
endif;
if ( ! function_exists( 'gssi_posted_in' ) ) :
// Prints HTML with meta information for the current post (category, tags and permalink).
function gssi_posted_in() {
	// Retrieves tag list of current post, separated by commas.
	$tag_list = get_the_tag_list( '', ', ' );
	if ( $tag_list ) {
		$posted_in = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'gssi' );
	} elseif ( is_object_in_taxonomy( get_post_type(), 'category' ) ) {
		$posted_in = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'gssi' );
	} else {
		$posted_in = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'gssi' );
	}
	// Prints the string, replacing the placeholders.
	printf(
		$posted_in,
		get_the_category_list( ', ' ),
		$tag_list,
		get_permalink(),
		the_title_attribute( 'echo=0' )
	);
}
endif;
// Disable the Admin Bar.
add_filter( 'show_admin_bar', '__return_false' );
// top level body class 
add_filter('body_class','top_level_parent_id_body_class');
function top_level_parent_id_body_class($classes) {
        global $wpdb, $post;
        if (is_page()) {
            if ($post->post_parent)    {
               $ancestors=get_post_ancestors($post->ID);
               $root=count($ancestors)-1;
               $parent = $ancestors[$root];
        } else {
               $parent = $post->ID;
        }
        $classes[] = 'top-pageid-' . $parent;
        }
        return $classes;
}
function get_top_ancestor($id) {
	$current = get_post($id);
	if(!$current->post_parent) {
		return $current->ID;
	}
	else {
		return get_top_ancestor($current->post_parent);
	}
}
function page_body_classes($classes) {
    global $wp_query;
    
    // if there is no parent ID and it's not a single post page, category page, or 404 page, give it
    // a class of "parent-page"
    if( $wp_query->post->post_parent < 1  && !is_single() && !is_archive() && !is_404() ) {
        $classes[] = 'parent-page';
    };
    
    // if the page/post has a parent, it's a child, give it a class of its parent name
    if($wp_query->post->post_parent > 0 ) {
        $parent_title = get_the_title($wp_query->post->post_parent);
        $parent_title = preg_replace('#\s#','-', $parent_title);
        $parent_title = strtolower($parent_title);
        $classes[] = 'parent-pagename-'.$parent_title;
    };
    
    // add a class = to the name of post or page
    $classes[] = $wp_query->queried_object->post_name;
    
    return array_unique($classes);
};
add_filter('body_class','page_body_classes');
//Conditional statement for all child pages
function is_tree( $pid ) {      // $pid = The ID of the page we're looking for pages underneath
    global $post;               // load details about this page
    if ( is_page($pid) )
        return true;            // we're at the page or at a sub page
    $anc = get_post_ancestors( $post->ID );
    foreach ( $anc as $ancestor ) {
        if( is_page() && $ancestor == $pid ) {
            return true;
        }
    }
    return false;  // we arn't at the page, and the page is not an ancestor
}
//Usage:  if ( is_tree('id') ){}
//Foxy shop filters
add_filter('foxyshop_category_children_write', 'my_foxyshop_category_children_write', 10, 2);
function my_foxyshop_category_children_write($liwrite, $term) {
    global $taxonomy_images_plugin, $post;
    
    if (substr($term->name,0,1) != "_") {
    	$url = get_term_link($term, "foxyshop_categories");
	    $liwrite = "";
	    $liwrite .= '<li class="pc-item" id="foxyshop_category_' . $term->term_id . '">';
	    if (isset($taxonomy_images_plugin)) {
	        $img = $taxonomy_images_plugin->get_image_html("full size", $term->term_taxonomy_id);
	        if(!empty($img)) $liwrite .= '<a href="' . $url . '" class="foxyshop_category_image">' . $img . '</a>';
	    }
	    $liwrite .= '<h2><a href="' . $url . '">' . $term->name . '</a></h2>';
	    if ($term->description) $liwrite .= apply_filters('the_content', $term->description);
	    
	    $liwrite .= '</li>'."\n";
	    
	    return $liwrite;
    }
    
}
//Foxy shop switch url
add_filter('foxyshop_breadcrumbs_base_link', 'my_foxyshop_breadcrumbs_base_link');
function my_foxyshop_breadcrumbs_base_link($str) {
    return get_bloginfo('url') . FOXYSHOP_URL_BASE . '/our-products/';
}
//AFC Options
if( function_exists('acf_add_options_page') ){
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Theme Header Settings',
	// 	'menu_title'	=> 'Header',
	// 	'parent_slug'	=> 'theme-general-settings',
	// ));
}
?>