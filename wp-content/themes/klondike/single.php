<?php

/**

 * The Template for displaying all single posts.

 *

 * @package WordPress

 * @subpackage Twenty_Ten

 * @since Twenty Ten 1.0

 */



get_header(); ?>



		<article id="container">

			<div>



<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>



				<div id="nav-above" class="navigation">

					<a href='/tech-resources/technews-blog/'>All Blog Posts</a>

				</div><!-- #nav-above -->



				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>



					<?php if ( has_post_thumbnail() ) {

						the_post_thumbnail();

					} ?>



					<div class='meta'><?php the_date('d F '); ?> <span>in</span> <?php echo get_the_category_list() ?></div>

					

					<div class="entry-content">

						<h1 class="entry-title"><?php the_title(); ?></h1>

						<?php the_content(); ?>

						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'gssi' ), 'after' => '</div>' ) ); ?>

					</div><!-- .entry-content -->



				<?php if ( get_the_author_meta( 'description' ) ) : // If a user has filled out their description, show a bio on their entries  ?>

					<div id="entry-author-info">

						<div id="author-avatar">

							<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'gssi_author_bio_avatar_size', 60 ) ); ?>

						</div><!-- #author-avatar -->

						<div id="author-description">

							<h2><?php printf( esc_attr__( 'About %s', 'gssi' ), get_the_author() ); ?></h2>

							<?php the_author_meta( 'description' ); ?>

							<div id="author-link">

								<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">

									<?php printf( __( 'View all posts by %s <span class="meta-nav">&rarr;</span>', 'gssi' ), get_the_author() ); ?>

								</a>

							</div><!-- #author-link	-->

						</div><!-- #author-description -->

					</div><!-- #entry-author-info -->

<?php endif; ?>



					

				</div><!-- #post-## -->



				

				



<?php endwhile; // end of the loop. ?>



			</div>

		</article><!-- #container -->





<?php get_footer(); ?>

