<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

get_header(); ?>


<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<h1 class="entry-title"><?php the_title(); ?></h1>

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
						<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-content -->
				</article><!-- #post-## -->
				
				
					<?php
						$featured_products = get_field('featured_products');

						if($featured_products){
							echo '<div id="featured-products" class="iao"><span class="heading">Featured Products</span><ul class="cf">';
							foreach ($featured_products as $featured_product) :
								echo "<li>";
									echo '<div class="fp-img-wrap">
											<a href="'. $featured_product['link'] .'"><img src="'.$featured_product['image']['url'].'"></a></div>';
									echo '<a href="'. $featured_product['link'] .'">'. $featured_product['title'] .'</a>';
								echo "</li>";
							endforeach;
							echo '</div></ul>';
						}
					?>
				
					<?php
						$products_applicables = get_field('products_applicable_to_your_industry');

						if($products_applicables){
							echo '<div id="applicable-products" class="iao"><span class="heading">Explore all products applicable to your industry:</span><ul class="cf">';
							foreach ($products_applicables as $products_applicable) :
								echo "<li>";
									echo '<a href="/product-cat/our_products/'.strtolower (str_replace(' ', '-', $products_applicable['title'] )).'/">'. $products_applicable['title'] .'</a>';
								echo "</li>";
							endforeach;

							echo '</div></ul>';
						}
					?>
					
				


<?php endwhile; ?>


<?php get_footer(); ?>
