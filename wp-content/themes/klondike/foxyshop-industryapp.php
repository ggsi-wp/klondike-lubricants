<?php 
/*
Template Name: Foxy Industry Applications Template
*/
?>

<?php get_header(); ?>

<?php foxyshop_include('header'); ?>
<div id="foxyshop_container" class="products-container">
	<?php

	//echo '<h1 id="foxyshop_category_title">Products</h1>';

	//Show all children that have a parent of 0 (top level ones)
	//Options: (Parent ID) (Show Product Count in Parentheses) <- Shows all child products (including sub categories)
	foxyshop_category_children(14, false);

	?>
</div>
<?php foxyshop_include('footer'); ?>

<script type="text/javascript">
jQuery(document).ready(function($){
	//This is set up for a three-column display. For a two column you need to do: nth-child(odd)
	$(".foxyshop_categories>li:nth-child(4n+1)").addClass("newrow");
	$(".foxyshop_categories").addClass("cf");
});
</script>

<?php get_footer(); ?>