<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
/*
Template Name: Homepage Template
*/

get_header(); ?>

<div id="main" class="page-section"><div class="m-w1"><div class="m-w2 section-inner cf">
	<!-- <a href="/product-cat/our_products/nano/ " id="logohome"></a><a href="https://klondikelubricants.com/tech-resources/video-gallery/  " id="home-video"></a> -->



	<div id="content-homepage"><div class="c-w1">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="post" id="post-<?php the_ID(); ?>">
			<div class="entry">
				<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>

			</div>

		</div>
		<?php endwhile; endif; ?>
	<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>


<?php get_footer(); ?>
