<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
/*
Template Name: Page with Accordion
*/

get_header(); ?>


<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<h1 class="entry-title"><?php the_title(); ?></h1>

		<div class="entry-content">
			<?php the_content(); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
			<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
		</div><!-- .entry-content -->
		<?php 
			$accordions = get_field('list');

			if($accordions){
		?>

		<div class="accord">
			<ul>
				<?php foreach($accordions as $accordion ):?>
				<li>
					<span class="accord-sec"><?php echo $accordion['list_title']; ?></span>
					<ul>						
						<li>
							<ul class="table-head">
								<li class="a-products"><span>Product</span></li>
								<li class="a-pds"><span>PDS English</span></li>
								<li class="a-pds-e"><span>PDS French</span></li>
								<li class="a-sds"><span>SDS English</span></li>
								<li class="a-sds-e"><span>SDS French</span></li>
							</ul>
							<?php foreach($accordion['list_item'] as $listItem): ?>
							<ul class="table-body">
								<li class="a-products"><span><?php echo $listItem['title']; ?></span></li>

								<li class="a-pds"><?php if($listItem['pds']): ?><a href="<?php echo $listItem['pds']; ?>" target="_blank">View</a><?php endif; ?></li>

								<li class="a-pds-e"><?php if($listItem['pds_french']): ?><a href="<?php echo $listItem['pds_french']; ?>" target="_blank">View</a><?php endif; ?></li>
						
								<li class="a-sds"><?php if($listItem['sds']): ?><a href="<?php echo $listItem['sds']; ?>" target="_blank">View</a><?php endif; ?></li>

								<li class="a-sds-e"><?php if($listItem['sds_french']): ?><a href="<?php echo $listItem['sds_french']; ?>" target="_blank">View</a><?php endif; ?></li>

							</ul>
							<?php endforeach; ?>
						</li>						
					</ul>
				</li>
			<?php endforeach;?>
			</ul>
		</div>
		<?php } ?>
	</article><!-- #post-## -->

<?php endwhile; ?>


<?php get_footer(); ?>
